// Copyright (C) 2016, 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.concurrent.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_net.NetRunner;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.*;

// =====

public enum MainThreads implements QIfs.TriggerIf, Runnable {

  // =====

  TOPNET;

  // Token-/Thread-Oriented Programming style (TOP), based on Petri nets.
  //
  // IDEA
  //
  // Combining Petri Place (QPlace) + Transition:
  // -- QDispatcher/ QSynchronizer (Place(s) with typed/guarded Transitions).
  // -- QProcessStep (Places with single Transition).
  // -- QStore (feeding itself)

  // Petri Arcs are implemented as simple pipes with a buffering end-point and a referencing
  // start-point.
  // Petri Places and Transitions are 'normalized' by specific element groups: Transposers and
  // Stores.
  // Transposers = active elements: Process steps, Dispatcher/ trigger, Terminator (sink/ source).
  // Places = buffered ports preceding the transitions, for the incoming data tokens (batons).
  // Transposers 'process' the data and pass the results on as in a relay race (contrary to callable
  // functions/ methods). They use the Token's values like arguments and the data in the Stores like
  // parameters.
  // Outgoing ports (= results) are implemented as a simple references to the succeeding in-port
  // buffers.
  // ==> No queue or extra synchronization for Places: one-to-one connections avoid internal race
  // conditions.
  // ==> Several such (Q)Nets can be connected via dedicated Places: Sinks and Sources.
  //
  // MODEL
  //
  // Place's buffer 'x' (in-port): buffering endpoint of Petri Arc for Petri Place 'O':
  // -- Process fires if all in-ports/ places have data.
  // -- Dispatcher collects data (unguarded) and passes it on according to Transition's guards.
  // -- Store is implemented as container with synchronized methods.
  // -- Sinks and sources are implemented as Dispatchers with dedicated/ exposed QPlaces.
  //
  // Process step;   Dispatcher/Synchronizer:    Store ('synchronized', feeding data tokens)
  //                             fast
  // ---x O -\            --x O ----|--->             <----.
  // .        \|--->           \       /         --x  v   /|--->
  // ---x O ---|           sync \--\|-/          ---x O -<
  // .        /|--->      ---x O ---|--->        --x  ^   \|--->
  // ---x O -/            ---x O --/|--->             <----'
  //                           ^
  //                           <--- pull
  //
  // IMPLEMENTATION
  //
  // Data tokens (QToken=QStack) do not carry the 'full load' of data, because the data gets
  // stored in stateful data containers: (Q)Stores.
  // -- (Q)Stores are stateful (e.g. backed by files),
  // -- (Q)Transposers are stateless.
  // -- Such Petri nets (QNets) get connected to the outside world via dedicated QPlaces.
  // Tokens and the Container entries have a timestamp, i.e. containers keep latest versions
  // or version history.
  // Process step is implemented as executable, with an extra scheduling step
  // that combines the incoming QTokens into a single task.

  // Consistency of the data is possible using the timestamps.
  // Due to parallel processing of tokens the timestamped data values may have 'glitches'
  // => Transposers and Stores use the timestamps to minimize non-deterministic values.
  // Java's package visibility utilized: all processes/ classes in a dedicated package run
  // on the same thread (exception: public methods) and do not need to synchronize
  // their data usage.

  ///// Threaded (in/ out/ trigger)

  public final QPlace wxGateIn = new QPlace();
  private final QPlace zPool = new QPlace();
  private QPlace rGateOut;

  /////

  private static final TopScheduler scheduler = new TopScheduler();

  static final QIfs.QRunnableIf[] kPrcsPool =
      new QIfs.QRunnableIf[] {
        WkRunner.INSTANCE,
        IoRunner.INSTANCE,
        NetRunner.INSTANCE,

        // For scheduler:
        null,

        ///// Ext, async

        ClickRepeater.INSTANCE,
      };

  private static ExecutorService[] zThreadPool = null;
  private static Future<QToken>[] zThreadPoolFutures = null;
  private static Future<QToken> zThreadPoolFutureDelayed4Scheduler = null;
  private static ExecWrapper[] zWrapped = null;

  // =====

  @SuppressWarnings("unchecked")
  public void init() {
    QOpFeed.zStack.jEnd = 0;
    QOpFeed.zStack.jStart = 0;
    if (null != zWrapped) {
      return;
    }
    for (int i0 = 0; i0 < kPrcsPool.length; ++i0) {
      if (null == kPrcsPool[i0]) {
        zWrapped = new ExecWrapper[i0];
        break;
      }
    }
    zThreadPool = new ExecutorService[kPrcsPool.length];
    zThreadPoolFutures = new Future[kPrcsPool.length];
    for (int i0 = 0; i0 < zWrapped.length; ++i0) {
      zThreadPool[i0] = Executors.newSingleThreadExecutor();
      zWrapped[i0] = new ExecWrapper(i0, kPrcsPool[i0]);
    }
    for (int i0 = zWrapped.length; i0 < zThreadPool.length; ++i0) {
      zThreadPool[i0] = Executors.newSingleThreadExecutor();
    }
  }

  public boolean reset(QPlace xrSink) {
    this.rGateOut = xrSink;
    return true;
  }

  // =====

  private static class ExecWrapper implements Callable<QToken> {

    // =====

    final QPlace wxTasks = new QPlace();

    // long startTime;

    private final QIfs.QRunnableIf mExec;
    private final int iRunnable;

    private ExecWrapper(int xIndex, QIfs.QRunnableIf exec) {
      this.mExec = exec;
      iRunnable = xIndex;
    }

    @Override
    public QToken call() {
      // startTime = DateFunc.currentTimeMillisLinearized();
      QToken out = null;
      QToken task = wxTasks.pull();
      if ((null == task) || (0 >= mExec.start(Dib2Root.app.qTick.get(), task))) {
        ExceptionAdapter.throwAdapted(
            new IllegalArgumentException(), getClass(), "Token = " + task);
      }
      try {
        while (true) {
          int cRemaining = zWrapped[iRunnable].mExec.stepAsync();
          if (0 >= cRemaining) {
            break;
          } else if (1 < cRemaining) {
            // TODO feedback
            continue;
          }
          cRemaining = zWrapped[iRunnable].mExec.step(Dib2Root.app.qTick.get());
          Thread.yield();
          if (0 >= cRemaining) {
            break;
          }
        }
        out = zWrapped[iRunnable].mExec.call();
      } catch (Throwable t0) {
        out = QToken.ERROR; // + cause
        ExceptionAdapter.throwAdapted(t0, getClass(), "from = " + iRunnable);
      }
      return out;
    }
  }

  // =====

  private static class TopScheduler implements Callable<QToken> {

    // =====

    private char zChanges = 0;

    private void cleanState() {
      for (int i0 = 0; i0 < zWrapped.length; ++i0) {
        if (null != zThreadPoolFutures[i0]) {
          if (zThreadPoolFutures[i0].isDone()) {
            collectResult(i0);
          }
        }
        zWrapped[i0].wxTasks.flush(true);
      }
      for (int i1 = 0; i1 < zThreadPool.length; ++i1) {
        if (i1 != zWrapped.length) {
          kPrcsPool[i1].removeWipData4Interrupts();
          continue;
        }
        for (int i0 = 0; i0 < QOpFeed.zStack.items.length; ++i0) {
          if ((QOpFeed.zStack.items[i0] instanceof QSeq)
              && (QOpMain.zzWIPCALC == (QIfs.QWordIf) ((QSeq) QOpFeed.zStack.items[i0]).at(0))) {
            if ((QOpFeed.zStack.jStart <= i0) && (i0 < QOpFeed.zStack.jEnd)) {
              QOpFeed.zStack.remove(i0 - QOpFeed.zStack.jStart);
            } else {
              QOpFeed.zStack.items[i0] = null;
            }
          }
        }
      }
      QToken.zWip.clear();
    }

    boolean isIdle() {
      for (int i0 = 0; i0 < zWrapped.length; ++i0) {
        if (null != zThreadPoolFutures[i0]) {
          // Result is pending
          return false;
        }
        if (null != zWrapped[i0].wxTasks.peek()) {
          return false;
        }
      }
      final long tick0 = Dib2Root.app.qTick.get();
      long tick1 = DateFunc.currentTimeNanobisLinearized(false);
      if (tick0 < tick1) {
        // Indirectly advance state of shared objects (comp.old = comp.current):
        Dib2Root.app.qTick.set(tick1);
        Dib2Root.app.qTickMin = tick1;
      }
      return (null == TOPNET.wxGateIn.peek()) && (null == TOPNET.zPool.peek());
    }

    private void processNSync() throws InterruptedException {
      long lastTime = DateFunc.currentTimeNanobisLinearized(false);
      isIdle();
      while (AppState.EXIT_DONE != Dib2Root.app.appState) {
        if (Dib2Root.app.appState.ordinal() == AppState.EXIT_REQUEST.ordinal()) {
          cancelAll(false);
          // Have UI thread handle the request:
          Dib2Root.platform.invalidate();
          return;
        }
        QToken task;
        while (null != (task = TOPNET.wxGateIn.pull())) {
          TOPNET.zPool.push(task);
        }
        while ((null != (task = TOPNET.zPool.pull()))) {
          if ((task == QToken.ERROR) || (null == task.op)) {
            TOPNET.rGateOut.push(task);
            continue;
          }
          if ((task.op instanceof QOpMain) && (null != ((QOpMain) task.op).delegated)) {
            task.op = ((QOpMain) task.op).delegated;
          }
          try {
            if (task.op instanceof QOpFeed) {
              QOpFeed.process(task);
              continue;
            } else if (task.op instanceof QOpMain) {
              // Shortcuts:
              if (QOpFeed.process4Stack(task)) {
                continue;
              }
            }
          } catch (Throwable t0) {
            cancelAll(false);
            ExceptionAdapter.throwAdapted(t0, getClass(), "" + task);
          }
          for (int iPrcs = 0; iPrcs < zWrapped.length; ++iPrcs) {
            if (zWrapped[iPrcs].mExec.guard(task)) {
              zWrapped[iPrcs].wxTasks.push(task);
              task = null;
              break;
            }
          }
          if (null != task) {
            TOPNET.rGateOut.push(task);
          }
        }
        for (int iPrcs = 0; iPrcs < zWrapped.length; ++iPrcs) {
          if ((null == zThreadPoolFutures[iPrcs]) && (null != zWrapped[iPrcs].wxTasks.peek())) {
            zThreadPoolFutures[iPrcs] = zThreadPool[iPrcs].submit(zWrapped[iPrcs]);
            if (Dib2Lang.AppState.ACTIVE.ordinal() <= Dib2Root.app.appState.ordinal()) {
              ++zChanges;
            }
          }
        }

        Thread.yield();
        final long time = DateFunc.currentTimeNanobisLinearized(false);
        if (Dib2Root.app.appState.ordinal() < AppState.EXIT_REQUEST.ordinal()) {
          // 1/8 sec:
          if ((time - lastTime) >= (1 << (30 - 3))) {
            TOPNET.prepareFeedShow();
            lastTime = DateFunc.currentTimeNanobisLinearized(false);
          } else {
            ((net.sf.dibdib.thread_feed.FeederRf) Dib2Root.app.feederCurrent).get().prepareFeed();
            // Some systems seem to use this for power saving ...?
            Thread.sleep(70);
          }
        }

        while (null != (task = TOPNET.wxGateIn.pull())) {
          TOPNET.zPool.push(task);
        }
        for (int i0 = 0; i0 < zWrapped.length; ++i0) {
          collectResult(i0);
        }
        if (isIdle()) {
          if ((5 < zChanges) && IoRunner.check4AutoSave()) {
            zChanges = 0;
            TOPNET.zPool.push(QToken.createTask(QOpFeed.zzSAV0));
          } else {
            break;
          }
        }
      }
      cleanState();
      if (Dib2Lang.AppState.ACTIVE.ordinal() >= Dib2Root.app.appState.ordinal()) {
        TOPNET.prepareFeedShow();
      }
    }

    @Override
    public QToken call() {
      QToken out = QToken.EMPTY;
      try {
        processNSync();
      } catch (InterruptedException e0) {
        out = null;
      }
      if (null == out) {
        cancelAll(false);
      } else {
        for (int i0 = zWrapped.length + 1; i0 < zThreadPool.length; ++i0) {
          collectResult(i0);
        }
      }
      return out;
    }
  }

  // =====

  public static boolean isIdle() {
    return scheduler.isIdle() && (null == TOPNET.wxGateIn.peek());
  }

  static void push(QToken xmToken) {
    TOPNET.zPool.push(xmToken);
  }

  private static QToken collectResult(int i0) {
    QToken out = null;
    if ((null != zThreadPoolFutures[i0]) && zThreadPoolFutures[i0].isDone()) {
      try {
        out = zThreadPoolFutures[i0].get();
      } catch (Throwable e0) {
        out = (i0 >= zWrapped.length) ? null : QToken.ERROR;
      } finally {
        zThreadPoolFutures[i0] = null;
      }
      if (null != out) {
        TOPNET.zPool.push(out);
      }
    }
    return out;
  }

  private static QToken cancelOne(int iRunnable) {
    Future<QToken> fut = zThreadPoolFutures[iRunnable];
    if (null == fut) {
      return null;
    }
    QToken out = null;
    try {
      if (null != fut) {
        out = fut.get(350, TimeUnit.MILLISECONDS);
      }
    } catch (Exception e0) {
      fut.cancel(true);
    } finally {
      zThreadPoolFutures[iRunnable] = null;
    }
    return out;
  }

  public static void cancelAll(boolean shutDown) {
    TOPNET.wxGateIn.flush(true);
    for (int i0 = 0; i0 < zWrapped.length; ++i0) {
      cancelOne(i0);
      if (shutDown) {
        zThreadPool[i0].shutdownNow();
      }
    }
    if (shutDown) {
      // Scheduler as the last one:
      for (int i0 = zThreadPool.length - 1; i0 >= zWrapped.length; --i0) {
        cancelOne(i0);
        // Going dead :-)
        zThreadPool[i0].shutdownNow();
      }
    } else {
      // Skipping the scheduler:
      for (int i0 = zWrapped.length + 1; i0 < zThreadPool.length; ++i0) {
        cancelOne(i0);
      }
    }
  }

  private static boolean prepareFeedShow_saved = false;
  private void prepareFeedShow() {
    if ((Dib2Root.app.feederNext != Dib2Root.app.feederCurrent)
        && (null != Dib2Root.app.feederNext)) {
      Dib2Root.app.feederCurrent = Dib2Root.app.feederNext;
      ((net.sf.dibdib.thread_feed.FeederRf) Dib2Root.app.feederCurrent).get().start();
      if (!prepareFeedShow_saved && (Dib2Root.app.feederNext == Dib2Root.app.mainFeeder)) {
        prepareFeedShow_saved = true;
        MainThreads.push(QToken.createTask(QOpFeed.zzSAV0));
      }
    }
    ((net.sf.dibdib.thread_feed.FeederRf) Dib2Root.app.feederCurrent).get().prepareFeed();
    Dib2Root.platform.invalidate();
  }

  public boolean prepareFeedNoTick() {
    if (isIdle()
        || (null == zThreadPoolFutures[zWrapped.length])
        || zThreadPoolFutures[zWrapped.length].isDone()) {
      // Run on UI thread, since nothing else active:
      prepareFeedShow();
      return true;
    }
    // Feed is being handled ...
    return false;
  }

  public static QToken runExt(QIfs.QRunnableIf prcs, QToken task) {
    for (int i0 = zWrapped.length; i0 < zThreadPool.length; ++i0) {
      if (prcs == kPrcsPool[i0]) {
        if (0 < prcs.start(Dib2Root.app.qTick.get(), task)) {
          return prcs.call();
        }
        return null;
      }
    }
    return null;
  }

  @Override
  public boolean triggerExt(QIfs.QRunnableIf prcs, QToken task) {
    for (int i0 = zWrapped.length; i0 < zThreadPool.length; ++i0) {
      if (prcs == kPrcsPool[i0]) {
        if (0 < prcs.start(Dib2Root.app.qTick.get(), task)) {
          zThreadPoolFutures[i0] = zThreadPool[i0].submit(prcs);
          return true;
        }
        return false;
      }
    }
    return false;
  }

  /** Run the scheduler via thread pool. */
  @Override
  public synchronized boolean trigger(QToken xOptionalCmd) {
    if (null != xOptionalCmd) {
      wxGateIn.push(xOptionalCmd);
    }
    if (Dib2Lang.AppState.DISCLAIMER.ordinal() >= Dib2Root.app.appState.ordinal()) {
      return false;
    }
    Future<QToken> fut = zThreadPoolFutures[zWrapped.length];
    if ((null == fut) || fut.isDone()) {
      if (null != fut) {
        try {
          fut.get();
        } catch (Exception e) {
        }
      }
      zThreadPoolFutures[zWrapped.length] = zThreadPoolFutureDelayed4Scheduler;
      zThreadPoolFutureDelayed4Scheduler = null;
    }
    if (null != zThreadPoolFutureDelayed4Scheduler) {
      // No need for extended queuing ...
      return false;
    }
    zThreadPoolFutureDelayed4Scheduler = zThreadPool[zWrapped.length].submit(scheduler);
    if (null == zThreadPoolFutures[zWrapped.length]) {
      zThreadPoolFutures[zWrapped.length] = zThreadPoolFutureDelayed4Scheduler;
      zThreadPoolFutureDelayed4Scheduler = null;
    }
    return true;
  }

  /** Run the scheduler on existing (service) thread, via overridden trigger(). */
  @Override
  public void run() {
    if (Dib2Root.app.appState.ordinal() > Dib2Lang.AppState.INIT.ordinal()) {
      scheduler.call();
    }
  }

  // =====
}
