// Copyright (C) 2021, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_net.QOpNet;
import net.sf.dibdib.thread_wk.*;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

// =====

public enum QOpFeed implements QIfs.QEnumIf {

  // =====

  // zzAUTOLOGIN,
  zzACCESSCODE,
  zzAPPLY, // apply operator on stack
  zzCHATACK,
  zzCHATADD,
  zzCHATADD4CHAT,
  zzCHATINVIT,
  zzCHATRECV,
  zzCHATRENAME,
  zzCHATSEND,
  // zzCHECK4UI,
  // zzCLICK,
  zzCONT2CHAT,
  zzCONTACTADD,
  zzEXEC, // execute 'as is'
  zzFILL,
  zzGET,
  zzGETFORCED,
  zzINIT_NEW,
  zzINIT_LOAD,
  zzPHRASE,
  zzPUSH,
  zzREFRESH,
  zzSAV0,
  zzSAV0_zzEXIT,

  /////

  ARCHIVE,
  CLRALL, // (0, 0, "clear all 'volatile' data (stack + memory)"),
  FSAVE,
  ;

  static QSTuple zStack = new QSTuple(null, 100);

  public static QIfs.QEnumIf[] create() {
    QOpMain.opsInternal.put("EXEC", zzEXEC);
    QOpMain.opsInternal.put("APPLY", zzAPPLY);
    QOpMain.opsInternal.put("GO", zzAPPLY);
    return values();
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }

  static boolean pullArguments(QToken task, int pushWip) {
    if (task.op instanceof QOpMain) {
      if ((null == task.argZ) && (3 <= ((QOpMain) task.op).cArgs)) {
        if (null == (task.argZ = zStack.pop(true))) {
          return false;
        }
      }
      if ((null == task.argY) && (2 <= ((QOpMain) task.op).cArgs)) {
        if (null == (task.argY = zStack.pop(true))) {
          return false;
        }
      }
      if ((null == task.argX) && (1 <= ((QOpMain) task.op).cArgs)) {
        if (null == (task.argX = zStack.pop(true))) {
          return false;
        }
      }
    }
    if (0 < pushWip) {
      final QSeq sq = (0 >= pushWip) ? null : task.pushWip4Seq(null);
      for (int i0 = 0; i0 < pushWip; ++i0) {
        zStack.push(sq);
      }
    }
    return true;
  }

  static boolean process4Stack(QToken task) {
    final int c0 = (task.op instanceof QOpMain) ? ((QOpMain) task.op).cReturnValues : 1;
    if (!pullArguments(task, c0)) {
      return true;
    }
    switch ((QOpMain) task.op) {
      case DUP:
        task.op = zzGETFORCED;
        if (task.argX instanceof QSeq) {
          task.wip = new QSeq[] {(QSeq) task.argX, (QSeq) task.argX};
          MainThreads.push(task);
          return true;
        }
        break;
      case SWAP:
        task.op = zzGETFORCED;
        if ((task.argX instanceof QSeq) && (task.argY instanceof QSeq)) {
          task.wip = new QSeq[] {(QSeq) task.argY, (QSeq) task.argX};
          MainThreads.push(task);
          return true;
        }
        break;
      case CLR3:
      case CLR2:
      case CLR1:
      case CLEAR:
        // Just drop the arguments:
        return true;
      case CLRN:
        if ((task.argX instanceof QWord) && ((QWord) task.argX).isNumeric()) {
          for (int i0 = (int) ((QWord) task.argX).i64(); i0 > 0; --i0) {
            zStack.pop(true);
          }
          return true;
        }
        break;
      case NOP:
        return true;
      default:
        ;
    }
    return false;
  }

  private static QToken addContactToChat(long hChatContacts, QToken cmd) {
    QSeq s0 = (QSeq) cmd.argX;
    long hChat = (null == s0) ? 0 : CcmSto.peek(s0, Cats.CHAT.flag, true);
    if (0 == hChat) {
      return null;
    }
    QIfs.QItemIf tup = CcmSto.peek(hChatContacts, false);
    String email = (tup instanceof QSTuple) ? ((QSeq) CcmTag.DAT.getValue((QSTuple) tup)).toStringFull() : null;
    if (null == email) {
      return null;
    } else if (email.contains(":GROUP")) {
      email = CcmSto.getTaggedValueOr(":GROUP:", email, "\n", "").replace('[', ' ').replace(']', ' ').trim();
    } else if (email.contains(":AT:")) {
      email = CcmSto.getFirstEmailAddress(email);
    } else {
      return null;
    }
    tup = CcmSto.peek(hChat, false);
    if (!(tup instanceof QSTuple) || !email.contains("@")) {
      return null;
    }
    String dat = ((QSeq) CcmTag.DAT.getValue((QSTuple) tup)).toStringFull();
    String[] all = email.split("[, \\[\\]]+");
    email = "";
    for (String sx : all) {
      if ((0 < sx.indexOf('@') && !dat.contains(sx))) {
        email += ", " + sx;
      }
    }
    if (2 >= email.length()) {
      return null;
    }
    email = email.substring(2);
    if (!dat.contains(":GROUP:")) {
      if (dat.contains(":AT:")) {
        return null;
      }
      dat = ":GROUP: []\n" + dat;
    }
    int iInsert = dat.indexOf(":GROUP:");
    for (iInsert += 5; iInsert < dat.length(); ++iInsert) {
      if ((dat.charAt(iInsert) < ' ') || (dat.charAt(iInsert) == ']')) {
        break;
      }
    }
    dat = dat.substring(0, iInsert) + " " + email + dat.substring(iInsert);
    //cmd.argX = QWord.createQWordInt(hChat);
    cmd.parN0 = hChat;
    cmd.argX = QSeq.createQSeq(dat);
    cmd.op = QOpWk.zzQUP;
    MainThreads.push(cmd);
    return null;
  }

  static void cleanWipData(boolean all) {
    while ((QWord.V_DOT == zStack.peek(0)) || (QWord.FALSE == zStack.peek(0))) {
      if (all || (QWord.V_DOT == zStack.peek(1)) || (QWord.FALSE == zStack.peek(1))) {
        zStack.pop(true);
      } else {
        break;
      }
    }
  }

  private static boolean trigger4Chat(QSeq xOptChat, QToken task, QEnumIf op, QSeq... args) {
    long hChat = (null != xOptChat) ? CcmSto.peek(xOptChat, Cats.CHAT.flag, true) : 0;
    QSeq chat = null;
    task.op = op;
    if ((null != xOptChat) && ((0 == hChat) || (0 >= Dib2Root.ccmSto.mUserAddr.indexOf('@')))) {
      return false;
    } else if (0 != hChat) {
      chat = QWord.createQWordInt(hChat);
    }
    task.argX = task.argY = task.argZ = chat;
    if (0 < args.length)
      task.argX = args[0];
    if (1 < args.length)
      task.argY = args[0];
    if (2 < args.length)
      task.argZ = args[0];
    cleanWipData(true);
    zStack.push(task.pushWip4Seq(QWord.V_0));
    MainThreads.push(task);
    return true;
  }

  static boolean process(QToken task) {
    QSeq vX = (task.argX instanceof QSeq) ? (QSeq) task.argX : QWord.NaN;
    QSeq vQ = Dib2Root.ccmSto.variable_get("Q");
    switch ((QOpFeed) task.op) {
      case zzCHATACK:
        return trigger4Chat(vQ, task, QOpMain.QMACK);
      case zzCHATADD4CHAT:
      case zzCHATADD: {
        String topic = vX.toStringFull(); // task.argX.toString();
        if ((null == topic) || (0 >= topic.length())) {
          return false;
        }
        if (0 >= topic.indexOf('@')) {
          final int nGroup = //((int)(MiscFunc.hash64_fnv1a(topic.getBytes(StringFunc.CHAR16UTF8), 32))) >>> 1;
              ((int) DateFunc.currentTimeMillisLinearized()) >>> 1;
          final QWord sLabel = QWord.createQWord("G" + nGroup, true);
          Dib2Root.ccmSto.variable_set("Q", sLabel);
          if (0 != CcmSto.peek(sLabel, Cats.CHAT.flag, false)) {
            return false;
          }
          String email = "[]";
          if (zzCHATADD4CHAT == task.op) {
            QSeq s0 = Dib2Root.ccmSto.variable_get("Q");
            if (null != s0) {
              long hChat = CcmSto.peek(s0, Cats.CHAT.flag, true);
              QIfs.QItemIf tup = CcmSto.peek(hChat, false);
              email = ((QSeq) CcmTag.DAT.getValue((QSTuple) tup)).toStringFull();
              if (email.contains(":AT:")) {
                email = "[" + CcmSto.getTaggedValueOr(":AT:", email, "\n", "") + "]";
              } else {
                email = CcmSto.getTaggedValueOr(":GROUP:", email, "\n", "[]");
              }
            }
          }
          task.parN0 = Cats.CHAT.flag;
          task.parS0 = "G" + nGroup;
          return trigger4Chat(null, task, QOpWk.zzQSTO, QSeq.createQSeq(":TOPIC: " + topic + "\n:GROUP: " + email));
        }
        task.parS0 = topic;
        // Fall through
      }
      case zzCONTACTADD: {
        ///// Actually creating a chat, but using this kind of chat will trigger the contact.
        String email = task.parS0;
        task.parN0 = Cats.CHAT.flag;
        task.parS0 = email;
        return trigger4Chat(null, task, QOpWk.zzQSTO, QSeq.createQSeq(":TOPIC: " + email.substring(0, email.indexOf('@')) + "\n:AT: " + email));
      }
      case zzCHATINVIT:
        return trigger4Chat(vQ, task, QOpMain.QMINVIT);
      case zzCHATRECV:
        return trigger4Chat(null, task, QOpNet.RCV, QWord.createQWord(3.0));//20 * 60.0));
      case zzCHATRENAME:
        if (task.argX instanceof QSeq) {
          return trigger4Chat(vQ, task, QOpMain.QUPTAGGED, (QSeq) task.argX, QWord.createQWord(":TOPIC:", true));
        }
        break;
      case zzCHATSEND: {
        String sx = (null == task.argX) ? null : task.argX.toString();
        QIfs.QSeqIf v2 = null;
        if ((null == sx) || (0 >= sx.trim().length())) {
          v2 = QOpFeed.zStack.peek(0);
          if (!(v2 instanceof QSeq) || (0 >= v2.toString().length())) {
            return false;
          }
        } else {
          v2 = QSeq.createQSeq(sx);
        }
        task.op = QOpMain.QMSEND;
        vX = null;
        return trigger4Chat(vQ, task, QOpMain.QMSEND, (QSeq)v2);
      }
      case zzACCESSCODE: {
          String path = IoRunner.check4Load();
          if (null != path) {
            ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).setPath(path);
            Dib2Root.app.feederNext = FeederRf.LOGIN;
            return true;
          }
        }
        return false;
      case zzCONT2CHAT: {
        return (null!=addContactToChat(task.parN0, task));
      }
      case zzPHRASE:
        {
          ((FeederRf.LoginFeeder) FeederRf.LOGIN.get()).requestPhrase();
          Dib2Root.app.feederNext = FeederRf.LOGIN;
        }
        return true;
      case zzINIT_LOAD: // DEL
        {
          task.op = QOpIo.zzLOAD_INITIAL;
          zStack.insert(0, task.pushWip4Seq(null));
          MainThreads.push(task);
        }
        return true;
      case zzSAV0:
      case zzSAV0_zzEXIT:
        {
          task.op = (QOpFeed.zzSAV0_zzEXIT == task.op) ? QOpWk.zzSAV1_zzEXIT : QOpWk.zzSAV1;
          task.argX = QSeq.NIL;
          task.pushWip4Seq(((QSTuple) zStack.clone()).items);
          MainThreads.push(task);
        }
        return true;
      case ARCHIVE:
        {
          task.op = QOpWk.zzARCHIVE;
          zStack.push(task.pushWip4Seq(((QSTuple) zStack.clone()).items));
          MainThreads.push(task);
        }
        return true;
      case FSAVE:
        {
          task.op = QOpWk.zzSAV1;
          if (null == task.argX) {
            task.argX = zStack.pop(true);
          }
          if (null != task.argX) {
            zStack.push(task.pushWip4Seq(((QSTuple) zStack.clone()).items));
            MainThreads.push(task);
          }
        }
        return true;
      case zzFILL:
        {
          QToken.zWip.remove(task.stamp);
          if (QOpMain.zzWIPCALC == (QIfs.QWordIf) ((QSeq) zStack.peek(0)).at(0)) {
            zStack.remove(0);
          } else if (0 < zStack.size()) {
            break;
          }
          if (null != task.wip) {
            for (QSeqIf val : (QSeqIf[]) task.wip) {
              if (null == val) {
                continue;
              }
              zStack.insert(0, val);
            }
          }
          if (Dib2Lang.AppState.ACTIVE.ordinal() > Dib2Root.app.appState.ordinal()) {
            Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
          }
          Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
        }
        return true;
      case zzPUSH:
        {
          final QSeqIf sq = task.argX;
          if (sq instanceof QSeqIf) {
            zStack.push(task.argX);
          }
        }
        return true;
      case zzAPPLY:
        {
          if (null == task.argX) {
            task.argX = zStack.pop(true);
            task.argX = (null == task.argX) ? QWord.V_BLANK : task.argX;
          }
          task.op =
              (task.argX instanceof QEnumIf)
                  ? (QEnumIf) task.argX
                  : Dib2Root.valueOfOr(task.argX.toString(), false, null);
          if (null == task.op) {
            break;
          }
          task.shiftArgs();
          final int c0 = (task.op instanceof QOpMain) ? ((QOpMain) task.op).cReturnValues : 1;
          if (!pullArguments(task, c0)) {
              return false;
          }
          MainThreads.push(task);
        }
        return true;
      case zzGETFORCED:
      case zzGET:
        {
          QToken.zWip.remove(task.stamp);
          int cTop = 0;
          QSeqIf el = null;
          if (0 != task.hWip) {
            for (; true; ++cTop) {
              el = zStack.peek(cTop);
              if (null == el) {
                break;
              } else if (task.hWip == el.getShash()) {
                break;
              }
            }
          }
        if ((null == el) && (zzGETFORCED != task.op)) {
          return false;
        }
          if (task.wip instanceof QSeq[][]) {
            final QSeq[][] seqs = (QSeq[][]) task.wip;
            for (QSeq[] dat : seqs) {
              final QSeqIf sq = (1 == dat.length) ? dat[0] : QSeq.createFlat(dat);
              if (null == el) {
                if (0 == cTop) {
                  zStack.push(sq);
                } else {
                  zStack.insert(-cTop, sq);
                }
              } else if (el == zStack.peek(cTop)) {
                zStack.replace(-1 - cTop, sq);
              } else {
                break;
              }
              ++cTop;
            }
          } else if (task.wip instanceof QSeq[]) {
            final QSeq[] seqs = (QSeq[]) task.wip;
            for (int i0 = seqs.length - 1; i0 >= 0; --i0, ++cTop) {
              final QSeq dat = seqs[i0];
              if (null == el) {
                if (0 == cTop) {
                  zStack.push(dat);
                } else {
                  zStack.insert(-cTop, dat);
                }
              } else if (el == zStack.peek(cTop)) {
                zStack.replace(-1 - cTop, dat);
              }
            }
          } else if (task.wip instanceof QSeq) {
            if (null == el) {
              zStack.push((QSeq) task.wip);
            } else {
              zStack.replace(-1 - cTop, (QSeq) task.wip);
            }
          }
        }
        return true;
      case CLRALL:
        {
          zStack = new QSTuple(null, 100);
        }
        return true;
      default:
        ;
    }
    if (!(task.op instanceof QOpFeed)) {
      MainThreads.push(task);
      return true;
    }
    return false;
  }

  // =====
}
