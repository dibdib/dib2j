// Copyright (C) 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_feed;

import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.ColorNmz.ColorDistinct;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_net.QOpNet;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.*;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

// =====

final class ChatFeeder extends FeederRf.GenericTextFeeder {

  // =====

  static class IntroChat extends FeederRf.GenericTextFeeder {

    public IntroChat(FeederRf owner) {
      super(owner);
    }

  }

  // =====

  long bMode_Adding = 0;
  
  public static final QIfs.QEnumIf[] kBarDelgChat = {
      QOpMain.LANG, QOpMain.VIEW, QOpMain.CLEAR, QOpFeed.zzAPPLY,
      //  QOpMain.STQ, QOpMain.QDFC,
      QOpFeed.zzCHATSEND, QOpFeed.zzCHATRECV, QOpFeed.zzCHATADD, QOpFeed.zzCONTACTADD,
      QOpFeed.zzCHATACK, QOpFeed.zzCHATINVIT, QOpFeed.zzCHATRENAME,
  };

  public ChatFeeder(FeederRf owner) {
    super(owner);
    cSlides = 5;
    jPage = 4;
  }

  @Override
  public FeederRf start() {
    super.start();
    bMode_Adding = 0;
    int page = (int) (nSlide30Supp >>> 30);
    UiValTag.kBarToolsDelegation = kBarDelgChat;
    UiValTag.kBarTools = UiValTag.kBarToolsChat;
    UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] = (1 >= page) ? ColorDistinct.PURE_RED.nmz.rgb0
        : ColorDistinct.APPLE_GREEN.nmz.rgb0;
    // Window not to be split:
    QToken token = QToken.createTask(QOpUi.zzSET, UiValTag.UI_PANE_SPLIT_GAP_X);
    token.parX = 0;
    UiPres.INSTANCE.wxGateIn4Feed.push(token);
    // Initialize messenger:
    token = QToken.createTask(QOpNet.zzINIT);
    Dib2Root.schedulerTrigger.trigger(token);
    return me;
  }

  @Override
  public long findSlideSupplement(int vNum, long bAbsRelSupp) {
    int old = (int) (nSlide30Supp >>> 30);
    long out = super.findSlideSupplement(vNum, bAbsRelSupp);
    int page = (int) (nSlide30Supp >>> 30);
    if ((page == 2) && (old == 3)) {
      out = super.findSlideSupplement(-1, 2L);
      page = (int) (nSlide30Supp >>> 30);
    }
    if (old != page) {
      UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] = (2 == page) ? ColorDistinct.APPLE_GREEN.nmz.rgb0
          : ColorDistinct.PURE_RED.nmz.rgb0;
    }
    return out;
  }

  private static int formatLines(String[] zFeedTxt, int end, String txt, int len, int xMaxLines, int perLine) {
    int count = 0;
    for (int i0 = 0; (i0 < len) && (count < xMaxLines); ++count) {
      int iMax = 10 + perLine + i0;
      // TODO: SH, ...
      int ix = i0;
      for (; (ix < iMax) && (ix < len); ++ix, iMax = 10 + perLine + i0) {
        if ('0' > txt.charAt(ix)) {
          if (' ' > txt.charAt(ix)) {
            iMax = ix++;
            break;
          } else if (ix >= (iMax - 12)) {
            iMax = ++ix;
            break;
          }
        }
      }
      ++end;
      if (0 < end) {
        iMax = (iMax <= len) ? iMax : len;
        zFeedTxt[end] = txt.substring(i0, iMax);
      }
      i0 = ix;
    }
    return end;
  }

  protected int fillTextLines4All(boolean filtered) {
    int page = (int) (nSlide30Supp >>> 30);
    final int supp = (int) (nSlide30Supp & 0x3fffffff);
    final int lines = linesPerSlide();
    final int avgWrapped = 60;
    final String displayEntry = UiPres.INSTANCE.getEntry(false).trim();
    zFeedTxt = (null == zFeedTxt) || (zFeedTxt.length <= lines) ? new String[lines + 1] : zFeedTxt;
    int end = (0 >= supp) ? 0 : -((supp - 1) * lines);
    QSTuple[] aMsgs = CcmSto.findEntries4Cats(Cats.MSG.flag);
    TreeMap<Long, QSTuple> msgs = new TreeMap<Long, QSTuple>();
    final String regex = filtered ? (".*" + displayEntry + ".*") : "";
    for (QSTuple msg : aMsgs) {
      try {
        if (filtered && (0 < displayEntry.length())
            && !((QSeq) CcmTag.DAT.getValue(msg)).toStringFull().contains(displayEntry)
            && !((QSeq) CcmTag.DAT.getValue(msg)).toStringFull().matches(regex)) {
          continue;
        }
      } catch (Exception e0) {
        // NOP
      }
      long vSort = (CcmTag.TIME.getWord((QSTuple) msg).getShash() >>> 1);
      for (QSTuple ovr = (QSTuple) msg; null != ovr; ++vSort) {
        ovr = msgs.put(-vSort, ovr);
      }
    }
    aMsgs = msgs.values().toArray(new QSTuple[0]);
    cSlides = aMsgs.length / lines + 5;
    int iMsg = (5 >= page) ? 0 : ((page - 5) * lines);
    boolean found = (0 >= supp);
    for (; (iMsg < aMsgs.length) && ((end + 1) < zFeedTxt.length); ++iMsg) {
      final QSTuple msg = (0 <= iMsg) ? aMsgs[iMsg] : null;
      if (null == msg) {
        continue;
      }
      long hCtrb = CcmTag.CNTRB.getWord(msg).i64();
      QIfs.QItemIf vCtrb = ((0 == hCtrb) || (Dib2Root.ccmSto.mUserPid == hCtrb)) ? QWord.V_DOT
          : ((1 == hCtrb) ? QWord.V_WILD : (CcmSto.peek(hCtrb, false)));
      String ctrb = (!(vCtrb instanceof QSTuple) || (null == ((QSTuple) vCtrb).items[0]))
          ? ((null == vCtrb) ? "*" : vCtrb.toString())
          : ((QSTuple) vCtrb).items[0].toString();
      QIfs.QItemIf ack = CcmTag.RECV.getValue((QSTuple) msg);
      String time = DateFunc.dateLocal4Hash62(CcmTag.TIME.getWord(msg).getShash()).substring(0, 16);
      String txt = ((QSeq) CcmTag.DAT.getValue(msg)).toStringFull();
      if (0 == supp) {
        txt = (avgWrapped < txt.length()) ? (txt.substring(0, avgWrapped - 5) + "...") : txt;
        zFeedTxt[++end] = (((ack instanceof QSeq) && ((QSeq) ack).toStringFull().contains("*:")) ? ".." : "")
            + time.substring(2).replace('T', '.').replaceAll("[^\\.0-9]", "") + "  "
            + ((4 < ctrb.length()) ? ctrb.substring(0, 4) : ctrb) + "\t" + txt;
        continue;
      }
      int cx = 2 + txt.length() / avgWrapped;
      if (end < -cx) {
        end += cx;
        continue;
        }
      cx += end;
      if ((++end) < zFeedTxt.length) {
        if (0 < end) {
          found = true;
          zFeedTxt[end] = "_____" + time + "___" + ctrb + "_________________________________";
        }
        end = formatLines(zFeedTxt, end, txt, txt.length(), zFeedTxt.length - end - 1, avgWrapped);
      }
      while (end < cx) {
          if ((++end) < zFeedTxt.length) {
          zFeedTxt[end] = "";
          }
        }
      }
    if (!found && (1 < supp)) {
      nSlide30Supp = ((nSlide30Supp >>> 30) << 30) | (supp - 1);
      return -1;
    }
    ++end;
    for (int i0 = (0 < end) ? end : 0; i0 < zFeedTxt.length; ++i0) {
      zFeedTxt[i0] = null;
    }
    zFeedTxt[0] = (4 < page) ? "@" : "FILTER\t@";
    return (end <= zFeedTxt.length) ? end : zFeedTxt.length;
  }

  protected int fillTextLines4Selected() {
    ///// Page 1: chats, 2: current messages of current chat, 3/4: overview.
    final String finalLine = "____________________________________________________________";
    int page = (int) (nSlide30Supp >>> 30);
    if (1 <= (nSlide30Supp & 0x3fffffff)) {
      // No supp pages for the active slides.
      nSlide30Supp = ((nSlide30Supp >>> 30) << 30);
    }
    final int lines = linesPerSlide();
    final int maxWrapped = (3 <= page) ? 1 : 10;
    zFeedTxt = (null == zFeedTxt) || (zFeedTxt.length <= lines) ? new String[lines + 1] : zFeedTxt;
    final String displayEntry = UiPres.INSTANCE.getEntry(false).trim();
    String preview = displayEntry;
    int end = 0;
    zFeedTxt[0] = "";
    QSeq s0 = Dib2Root.ccmSto.variable_get("Q");
    long hChat = (null != s0) ? CcmSto.peek(s0, Cats.CHAT.flag, true) : 0;
    if (0 == hChat) {
      if (0 >= Dib2Root.ccmSto.mUserAddr.indexOf('@')) {
        if (2 == page) {
          nSlide30Supp = 3L << 30;
          page = 3;
        }
      } else {
        s0 = QWord.createQWord(Dib2Root.ccmSto.mUserAddr, true);
        hChat = CcmSto.peek(s0, Cats.CHAT.flag, true);
      }
    }

    final int cStack = QOpFeed.zStack.size();
    final String xDraft = ((1 > cStack) ? "" : QOpFeed.zStack.at(cStack - 1).toString());
    preview = StringFunc.string4Mnemonics1345((0 < displayEntry.length()) ? preview : xDraft, 0L);
    if (1 >= page) {
      if (0 != (1L & bMode_Adding)) {
        zFeedTxt[++end] = "Tap chat to add addresses to " + s0;
      } else {
        zFeedTxt[++end] = "DRAFT\t" + xDraft;
        zFeedTxt[++end] = "PAR2\t" + ((2 > cStack) ? "" : QOpFeed.zStack.at(cStack - 2).toString());
      }
      final String fp = QOpNet.getFingerPrint();
      zFeedTxt[++end] = (null == fp) ? "" : ("____________________" + fp);
    } else if (3 == page) {
      String ed = "" + (long) (1000 * DateFunc.currentTimeEraDay());
      ed = (3 >= ed.length()) ? "*" : (ed.substring(0, ed.length() - 3) + "." + ed.substring(ed.length() - 3));
      zFeedTxt[++end] = "\t" + DateFunc.dateLocal4Millis(false) + "\t" + ed;
    }

    boolean expanded = (2 <= page);
    QSTuple[] aChats = (2 <= page) ? new QSTuple[0] : CcmSto.findEntries4Cats(Cats.CHAT.flag);
    TreeMap<Long, QSTuple> chats = new TreeMap<Long, QSTuple>();
    int i1 = 0;
    for (; i1 < aChats.length; ++i1) {
      QSTuple chat = aChats[i1];
      if ((null == chat) || ((chat.getShash() == hChat))) {
        continue;
      }
      long vSort = CcmTag.TIME.getWord(chat).getShash() >>> 1;
      for (QSTuple ovr = (QSTuple) chat; null != ovr; ++vSort) {
        ovr = chats.put(-vSort, ovr);
      }
    }
    // TODO Calculate offset ...
    aChats = chats.values().toArray(new QSTuple[0]);
    i1 = -1;
    for (; i1 < aChats.length; ++i1) {
      QSTuple chat = (0 > i1) ? (QSTuple) CcmSto.peek(hChat, false) : aChats[i1];
      if ((end + maxWrapped) >= zFeedTxt.length) {
        break;
      } else if ((null == chat) || ((chat.getShash() == hChat) && (0 <= i1))) {
        if ((0 > i1) && (0 <= end) && (null != s0)) {
          zFeedTxt[end] = s0.toString() + "...";
        }
        continue;
      }
      final String lbl = CcmTag.LABEL.getWord(chat).toString();
      final String timeChat = DateFunc.dateLocal4Hash62(CcmTag.TIME.getWord(chat).getShash());
      final String dat = ((QSeq) CcmTag.DAT.getValue(chat)).toStringFull();
      if ((0 <= i1) && (3 > page) && (0 < displayEntry.length())) {
        if (!lbl.contains(displayEntry) && !timeChat.contains(displayEntry)) {
          if ((3 > displayEntry.length()) || !dat.contains(displayEntry)) {
            continue;
          }
        }
      }
      String name = CcmSto.getTaggedValueOr(":TOPIC:", dat, "\n", "");
      name = (0 < name.length()) ? name : lbl;
      ++end;
      if (4 <= page) {
        zFeedTxt[end] = "";
      } else {
        zFeedTxt[end] = name + '\t' + timeChat;
        if ((0 < lbl.indexOf('@')) && !dat.contains(":GROUP:")) {
          long hContact = CcmSto.ensureContact4Chat(chat.getShash(), false, false);
          if (0 != hContact) {
            final String cd = ((QSeq) CcmTag.DAT.getValue((QSTuple) CcmSto.peek(hContact, false))).toStringFull();
            ++end;
            if (0 < end) {
              String fp = CcmSto.getTaggedValueOr(":FP:", dat, "\n", "");
              if (8 > fp.length()) {
                fp = "?" + CcmSto.getTaggedValueOr(":FP:", cd, "\n", "");
              } else if (!cd.contains(fp)) {
                fp = "?" + fp;
              }
              zFeedTxt[end] = "    " + lbl + '\t' + fp;
            }
          }
        } else {
          ++end;
          if (0 < end) {
            zFeedTxt[end] = "    " + lbl + '\t' + CcmSto.getTaggedValueOr(":GROUP:", dat, "\n", "");
          }
        }
      }
      if ((!expanded) || (0 == hChat)) {
        continue;
      }
      HashSet<Long> ghMsgs = CcmSto.findMsgs4Chat(hChat);
      if (4 == page) {
        final QSTuple[] msgs = CcmSto.findEntries4Cats(Cats.MSG.flag);
        for (QSTuple msg : msgs) {
          ghMsgs.add(msg.getShash());
        }
      }
      TreeMap<Long, QSTuple> msgs = new TreeMap<Long, QSTuple>();
      for (Long h0 : ghMsgs) {
        final QIfs.QItemIf mpg = CcmSto.peek(h0, false);
        if (!(mpg instanceof QSTuple)) {
          continue;
        }
        //QIfs.QItemIf ack = (null != mpg) ? CcmTag.RECV.getValue((QSTuple) mpg) : mpg;
        long vSort = (CcmTag.TIME.getWord((QSTuple) mpg).getShash() >>> 1);
        for (QSTuple ovr = (QSTuple) mpg; null != ovr; ++vSort) {
          ovr = msgs.put(-vSort, ovr);
        }
      }
      QSTuple[] aMsgs = msgs.values().toArray(new QSTuple[0]);
      int iMsg = ((0 < preview.length()) && !".".equals(preview) && (3 > page)) ? -1 : 0;
      final int perLine = (3 <= page) ? 72 : 45;
      for (; iMsg < aMsgs.length; ++iMsg) {
        final QSTuple msg = (0 <= iMsg) ? aMsgs[iMsg] : null;
        long hCtrb = (null != msg) ? CcmTag.CNTRB.getWord(msg).i64() : 0;
        QIfs.QItemIf vCtrb = ((0 == hCtrb) || (Dib2Root.ccmSto.mUserPid == hCtrb)) ? QWord.V_DOT
            : ((1 == hCtrb) ? QWord.V_WILD : (CcmSto.peek(hCtrb, false)));
        String ctrb = (!(vCtrb instanceof QSTuple) || (null == ((QSTuple) vCtrb).items[0]))
            ? ((null == vCtrb) ? "*" : vCtrb.toString())
            : ((QSTuple) vCtrb).items[0].toString();
        QIfs.QItemIf ack = (null != msg) ? CcmTag.RECV.getValue((QSTuple) msg) : msg;
        String time = (null == msg) ? ""
            : DateFunc.dateLocal4Hash62(CcmTag.TIME.getWord(msg).getShash()).substring(0, 16);
        boolean ackDone = (null == msg) || !((ack instanceof QSeq) && ((QSeq) ack).toStringFull().contains("*:"));
        String hd = "__" + ((null != msg) ? (".".equals(ctrb) ? "" : ctrb)
            : ("_________" + ((preview == displayEntry) ? "PREVIEW" : "DRAFT")));
        hd = "____" + (ackDone ? "" : "..") + hd;
        String txt = (null != msg) ? ((QSeq) CcmTag.DAT.getValue(msg)).toStringFull() : preview;
        if ((maxWrapped * perLine) < txt.length()) {
          int at = ((0 < iMsg) ? maxWrapped : (lines / 2 - 2)) * perLine;
          if (at < txt.length()) {
            txt = txt.substring(0, at - 7) + "...";
          }
        }
        if ((end + 2 + (txt.length() / perLine)) >= zFeedTxt.length) {
          break;
        }
        if (3 <= page) {
          if ((0 < displayEntry.length()) && (!txt.contains(displayEntry))) {
            continue;
          }
          ++end;
          if (0 < end) {
            zFeedTxt[end] = (2 >= time.length()) ? ""
                : time.substring(2).replace('T', '.').replaceAll("[^\\.0-9]", "")
                    + " " + ((4 < ctrb.length()) ? ctrb.substring(0, 4) : ctrb) + '\t' + txt;
          }
          continue;
        }
        hd = hd + "___" + time + finalLine;
        hd = ('_' == hd.charAt(70)) ? hd.substring(0, 70) : hd;
        int len = txt.length();
        ++end;
        if (0 < end) {
          zFeedTxt[end] = hd;
        }
        int cMax = (maxWrapped < zFeedTxt.length - end) ? maxWrapped : (zFeedTxt.length - end - 1);
        end = formatLines(zFeedTxt, end, txt, len, cMax, perLine);

        if (0 > iMsg) {
          zFeedTxt[++end] = finalLine;
          zFeedTxt[++end] = "_____";
        }
      }
      ++end;
      if ((iMsg >= aMsgs.length) && (0 < end) && (end < zFeedTxt.length)) {
        zFeedTxt[end] = finalLine;
      }
    }

    ++end;
    end = (end < zFeedTxt.length) ? end : zFeedTxt.length - 1;
    int i0 = zFeedTxt.length - 1;
    for (; i0 > end; --i0) {
      zFeedTxt[i0] = null;
    }
    zFeedTxt[0] = (2 == page) ? "@" : "FILTER\t@";
    for (; i0 > 0; --i0) {
      zFeedTxt[i0] = StringFunc.makePrintable(zFeedTxt[i0]);
    }
    return end;
  }

  @Override
  protected int prepareTextLines() {
    int page = (int) (nSlide30Supp >>> 30);
    QOpFeed.cleanWipData(false);
    int out = 0;
    if (4 > page) {
      out = fillTextLines4Selected();
    } else {
      out = fillTextLines4All(4 >= page);
      if (0 > out) {
        // Again, after correcting the page count.
        out = fillTextLines4All(4 >= page);
      }
    }
    return out;
  }

  @Override
  public QToken tryOrFilter4Ui(QToken task) {
    final int page = (int) (nSlide30Supp >>> 30);
    if ((QOpUi.zzKEY == task.op) && (StringFunc.STEP == task.parX)) {
      // Tapped.
      task = super.tryOrFilter4Ui(task);
      if (null != task) {
        int line = (ClickRepeater.qPointerY0 - UiValTag.UI_PANE_OFFS_Y.i32(0))
            / UiValFeedTag.UI_LINE_SPACING_PT10.i32(0);
        if ((5 >= line) && (0 != (1L & bMode_Adding))) {
          bMode_Adding = 0;
          ClickRepeater.qPointerY0 = -1;
        }
        if ((3 < line) && (line < (zFeedTxt.length - 1)) && (null != zFeedTxt[line])
            && (nPointerSlide == nSlide30Supp)) {
          if (!zFeedTxt[line].startsWith("  ")) {
            ++line;
            if ((null == zFeedTxt[line]) || !zFeedTxt[line].startsWith("  ")) {
              return null;
            }
          }
          //++line;
          if ((null != zFeedTxt[line]) && (0 < zFeedTxt[line].indexOf('\t'))) {
            String chatLabel = zFeedTxt[line].substring(0, zFeedTxt[line].indexOf('\t')).trim();
            QSeq cs = QWord.createQWord(chatLabel, true);
            if (0 != (1L & bMode_Adding)) {
              long hChat = (null != chatLabel) ? CcmSto.peek(cs, Cats.CHAT.flag, true) : 0;
              if (0 != hChat) {
                task.op = QOpFeed.zzCONT2CHAT;
                task.parN0 = hChat;
                task.argX = Dib2Root.ccmSto.variable_get("Q");
                return task;
              }
              bMode_Adding = 0;
            }
            // TODO: via task
            Dib2Root.ccmSto.variable_set("Q", cs);
            if (0 != CcmSto.peek(cs, Cats.CHAT.flag, true)) {
              findSlideSupplement(2, 1L);
            }
            ClickRepeater.qPointerY0 = -1;
          }
        }
        return null;
      }
    }
    if (null == task) {
      return null;
    }
    if (task.op instanceof QOpMain) {
      bMode_Adding = 0;
    }
    if (task.op instanceof QOpFeed) {
      bMode_Adding = 0;
      switch ((QOpFeed) task.op) {
        case zzPUSH:
          if ((StringFunc.CR == task.parN0) || (StringFunc.LF == task.parN0)) {
            task.op = QOpUi.zzKEY;
          }
          return task;
        case zzCHATADD:
          task.op = (page == 2) ? QOpFeed.zzCHATADD4CHAT : QOpFeed.zzCHATADD;
          return task;
        case zzCONTACTADD:
          String email = (null == task.argX) ? null : task.argX.toString();
          if ((null == task.argX) || (3 >= task.argX.toString().length())) {
            if (1 == page) {
              // Just switch mode:
              QSeq s0 = Dib2Root.ccmSto.variable_get("Q");
              long hChat = (null != s0) ? CcmSto.peek(s0, Cats.CHAT.flag, true) : 0;
              if (0 != hChat) {
                QIfs.QItemIf tup = CcmSto.peek(hChat, false);
                if ((tup instanceof QSTuple)
                    && ((QSeq) CcmTag.DAT.getValue((QSTuple) tup)).toStringFull().contains(":GROUP:")) {
                  bMode_Adding ^= 1L;
                }
              }
              return null;
            }
          }
          if (0 >= email.length()) {
            return null;
          }
          if (0 >= email.indexOf('@')) {
            email += "@x.x";
          }
          final QWord sLabel = QWord.createQWord(email, true);
          long hChat = CcmSto.peek(sLabel, Cats.CHAT.flag, false);
          if (0 != hChat) {
            if (2 == page) {
              task.op = QOpFeed.zzCONT2CHAT;
              task.argX = Dib2Root.ccmSto.variable_get("Q");
              task.parN0 = hChat;
              return task;
            }
            return null;
          }
          task.parS0 = email;
          return task;
        default:
          ;
      }
    }
    return task;
  }

  // =====
}
