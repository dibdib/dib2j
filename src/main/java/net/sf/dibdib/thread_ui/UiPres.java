// Copyright (C) 2019, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_ui;

import static net.sf.dibdib.thread_any.StringFunc.*;
import static net.sf.dibdib.thread_ui.UiValTag.kBarTools;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.Locale;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.ColorNmz.ColorDistinct;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.generic.QToken.QScript;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_net.QOpNet;

// =====

/** For platform's UI thread. */
public enum UiPres {

  // =====

  INSTANCE;

  ///// Threaded (in/ out/ trigger)

  public final QPlace wxGateIn4Feed = new QPlace();

  /////

  public volatile int zUiKeypadInx = 1;

  static volatile QScript qUiBarTitle;
  static volatile QScript qUiBarTools;
  static volatile QScript qUiBarEntry;
  static volatile QScript qUiBarStatus;
  static volatile QScript qUiKeypad;

  /** The text entry field (mnemonic), requires ziEntry to always stay in range. */
  private String zEntry = "";

  private int ziEntry = 0;

  private int zUiKeypadLastInx = 1;

  private int ziButtonFocus = 0;
  private boolean zbSlideNum = false;

  // =====

  public boolean init() {
    return true;
  }

  public String setEntry(String entry) {
    entry = StringFunc.mnemonics4String(entry, false);
    String out = zEntry;
    ziEntry = 0;
    zEntry = entry;
    ziEntry = entry.length();
    return out;
  }

  public String getEntry(boolean printable) {
    return printable ? zEntry : StringFunc.string4Mnemonics1345(zEntry, 0L);
  }

  private QToken createToken4Entry(QEnumIf xOperator, boolean asOperation) {
    QEnumIf operator = xOperator;
    String txt = zEntry;
    if (null != operator) {
      if ((QOpMain.VIEW == operator) || (QOpMain.LANG == operator)) {
        txt = "-1";
        xOperator = null;
      }
    } else if ((0 >= zEntry.length()) || !asOperation) {
      operator =
          asOperation ? QOpFeed.zzAPPLY : ((0 >= zEntry.length()) ? QOpMain.NOP : QOpFeed.zzPUSH);
    } else {
      operator = Dib2Root.valueOfOr(zEntry, asOperation, QOpFeed.zzPUSH);
    }
    // Index first:
    ziEntry = 0;
    zEntry = "";
    if (null != txt) {
      // No list processing yet
      txt = StringFunc.makePrintable(txt);
    }
    if ((null != txt)
        && (null == xOperator)
        && (1 < txt.length())
        && (('@' == txt.charAt(0)) || (':' == txt.charAt(0)))) {
      operator = ('@' == txt.charAt(0)) ? QOpMain.MMLD : QOpMain.MMSTO;
      txt = txt.substring(1);
    }
    final QToken out =
        ((null == txt) || (0 >= txt.length()))
            ? QScript.createTask(operator)
            : QScript.createTask(operator, QSeq.createQSeq(txt));
    if ((QOpFeed.zzAPPLY == operator) && (out.argX instanceof QIfs.QEnumIf)) {
      out.op = (QEnumIf) out.argX;
      out.argX = out.argY;
    }
    if ((null == out.argX) && (0 < txt.length())) {
      out.argX = QSeq.createQSeq(txt);
    }
    out.parS0 = txt;
    return out;
  }

  private boolean prepareFeed() {
    final long tick = DateFunc.currentTimeNanobisLinearized(true);
    UiValTag.tick(tick);
    // TODO as task after calculating temp/initial values ...
    return MainThreads.TOPNET.prepareFeedNoTick();
  }

  /**
   * Get command or key for mouse click.
   *
   * @param xX UI's x
   * @param xY UI's y
   * @return command or key for command or key for entry.
   */
  QToken checkUiEventMouse(int xXPx, int xYPx) {
    int xX = UiFrame.shift4Px(xXPx);
    int xY = UiFrame.shift4Px(xYPx);
    zbSlideNum = false;
    QEnumIf operator = null;
    final int bar = UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
    final int sbar = UiValTag.UI_WIN_HEIGHT.i32Fut() - bar;
    if (((Dib2Constants.UI_FRAME_BARS - 1) * bar) > xY) {
      if (bar > xY) {
        // Title bar.
        final int jX = Dib2Constants.UI_WIN_MARGIN;
        final int e0 = UiValFeedTag.UI_LINE_SPACING_PT10.i32(UiValTag.getTick());
        final int dsp = UiValTag.UI_WIN_WIDTH.i32Fut();
        final int inx = ((dsp >> 1) > xX) ? ((xX - jX) / e0) : (-1 - ((dsp - jX - xX) / e0));
        if ((-(UiValTag.kBarTitle.length >> 2) > inx)
            || (inx >= (UiValTag.kBarTitle.length >> 2))) {
          return null;
        }
        char key =
            (char)
                UiValTag.kBarTitle[
                    1 + ((0 <= inx) ? (inx << 1) : (UiValTag.kBarTitle.length + inx + inx))];
        operator = (StringFunc.ESCAPE == key) ? QOpMain.ESCAPE : QOpUi.zzKEY;
        if (key < ' ') {
        } else if ('=' == key) {
          // Menu
          operator = null; // TODO
          // Currently, only for toggling dark mode:
          ColorNmz.setDisplayMode(ColorNmz.isDarkMode() ? 1 : 0);
        } else if ('A' == key) {
          // Keyboard switch
          UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard] =
              (ColorDistinct.APPLE_GREEN.nmz.rgb0
                      == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])
                  ? ColorDistinct.MANGO.nmz.rgb0
                  : (((ColorDistinct.MANGO.nmz.rgb0
                          == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])
                      ? ColorDistinct.PURE_RED.nmz.rgb0
                      : ((ColorDistinct.PURE_RED.nmz.rgb0
                          == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])
                          ? ColorDistinct.SEA.nmz.rgb0
                          : ColorDistinct.APPLE_GREEN.nmz.rgb0)));
          ClickRepeater.uiCPointerMoving = false;
          return null;
        }
        if (null == operator) {
          return null;
        }
        QToken out = QToken.createTask(operator);
        out.parX = key;
        return out;
      } else if ((2 * bar) >= xY) {
        // Tool bar.
        final int jX = Dib2Constants.UI_WIN_MARGIN;
        final int e0 = UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
        final int inx = (xX - jX) / e0;
        if (0 > inx) {
          return null;
        }
        if ((2 >= inx) && ((2 > inx) || UiValTag.kBarTools[2].startsWith("BS"))) {
          final QToken task =
              QScript.createTask(
                  (1 == inx) ? QOpUi.VIEW : ((0 == inx) ? QOpUi.LANG : QOpUi.BAS2));
          return checkUiEvent(task);
        } else {
          operator = null;
          if ((null != UiValTag.kBarToolsDelegation) && (inx < UiValTag.kBarToolsDelegation.length)) {
            operator = UiValTag.kBarToolsDelegation[inx];
          }
          if (null == operator) {
            String name = UiValTag.kBarTools[inx].trim();
            operator = "GO".equals(name) ? QOpFeed.zzAPPLY : Dib2Root.valueOfOr(name, false, null);
          }
        }
        return (null != operator) ? createToken4Entry(operator, true) : null;
      }
      // Entry bar.
      if (((UiValTag.UI_WIN_WIDTH.i32Fut() * 7) >> 3) < xX) {
        if (0 < zEntry.length()) {
          return createToken4Entry(null, false);
        } else {
          return checkUiEventCtrl(PUSH);
        }
      } else if ((UiValTag.UI_WIN_WIDTH.i32Fut() >> 3) > xX) {
        UiFrame.shiftPointerCanvas4Page(-1, -1);
        ziEntry = 0;
      } else if (((UiValTag.UI_WIN_WIDTH.i32Fut() * 5) >> 3) < xX) {
        UiFrame.shiftPointerCanvas4Page(-1, -1);
        ziEntry = zEntry.length();
      } else {
        ClickRepeater.uiCPointerMoving = !ClickRepeater.uiCPointerMoving;
      }
      return null;
    } else if (sbar < xY) {
      // Status bar.
      int inx = xX * 15 / UiValTag.UI_WIN_WIDTH.i32Fut();
      if (12 <= inx) {
        if (0 < zEntry.length()) {
          return createToken4Entry(null, false);
        } else {
          return checkUiEventCtrl(PUSH);
        }
      } else if (7 == inx) {
        zbSlideNum = true;
      } else if (0 < inx) {
        final int delta = (7 <= inx) ? 1 : -1;
        ((FeederRf) (Dib2Root.app.feederCurrent))
            .get()
            .findSlideSupplement(delta, ((6 <= inx) && (inx <= 8)) ? 4L : 2L);
      }
      return null;
    }

    ///// Canvas/ keypad.

    boolean pointerMoved = false;
    boolean clicked = false;
    char k0 = 0;
    // final int zoomLevel = UiValTag.UI_ZOOMLVL_CANVAS.i32Fut();
    final boolean active = (ColorDistinct.APPLE_GREEN.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])
        || (ColorDistinct.SEA.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard]);
    if (ClickRepeater.uiCPointerMoving) {
      if (active) {
        ClickRepeater.uiCPointerMoving = false;
        UiFrame.shiftPointerCanvas4Page(xXPx, xYPx); //xX, xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar);
        return null;
      }
      int midX = UiValTag.UI_WIN_WIDTH.i32Fut();
      int midY = UiValTag.UI_WIN_HEIGHT.i32Fut();
      midX = 1 + (midX >>> 1);
      midY = 1 + ((midY + (2 * bar)) >>> 1);
      final int eX = (xX - midX) * 3 / midX;
      final int eY = (xY - midY) * 3 / midY;
      if ((0 == eX) && (0 == eY)) {
        ClickRepeater.uiCPointerMoving = false;
        return null;
      }
      k0 = ClickRepeater.cmd4MovePointer(eX, eY);
      pointerMoved = true;
    } else {
      int n8Y =
          (((xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar) * (UiValTag.qcKeys4Win + 1)) << 3)
              / (sbar - (Dib2Constants.UI_FRAME_BARS - 1) * bar);
      int n8X = ((xX * (UiValTag.qcKeys4Win + 1)) << 3) / UiValTag.UI_WIN_WIDTH.i32Fut();
      if ((n8Y < 4) || (((UiValTag.qcKeys4Win << 3) + 4) < n8Y) || (n8X < 4)
          || (((UiValTag.qcKeys4Win << 3) + 4) < n8X)) {
        // Scroll bar.
        n8Y /= (UiValTag.qcKeys4Win + 1);
        n8X /= (UiValTag.qcKeys4Win + 1);
        if ((0 >= n8X) && (0 >= n8Y)) {
          k0 = StringFunc.SCROLL_UP;
        } else if ((0 >= n8X) && (7 <= n8Y)) {
          k0 = StringFunc.SCROLL_LEFT;
        } else if ((7 <= n8X) && (0 >= n8Y)) {
          k0 = StringFunc.SCROLL_RIGHT;
        } else if ((7 <= n8X) && (7 <= n8Y)) {
          k0 = StringFunc.SCROLL_DOWN;
        } else if ((7 <= n8X) || (7 <= n8Y)) {
          double v8Y = 8.0 *
              ((xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar) * (UiValTag.qcKeys4Win + 1))
              / (sbar - (Dib2Constants.UI_FRAME_BARS - 1) * bar) / (UiValTag.qcKeys4Win + 1);
          double v8X = 8.0 * (xX * (UiValTag.qcKeys4Win + 1)) / (double) UiValTag.UI_WIN_WIDTH.i32Fut()
              / (UiValTag.qcKeys4Win + 1);
          if (7 <= n8X) {
            double vY = (v8Y - 1.2) / (8 - 2.5);
            vY = (0 >= vY) ? 0.0 : ((1.0 <= vY) ? 1.0 : vY);
            UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(0);
            final int max = Dib2Constants.UI_PAGE_HEIGHT - (UiValTag.UI_WIN_HEIGHT.i32Fut() - 3 * bar);
            if (0 < max) {
              UiValTag.UI_PANE_OFFS_Y.setFut((int) (max * vY));
            }
            return null;
          } else if (7 <= n8Y) {
            double vX = (v8X - 1.2) / (8 - 2.5);
            vX = (0 >= vX) ? 0.0 : ((1.0 <= vX) ? 1.0 : vX);
            UiValTag.UI_PANE_SPLIT_GAP_X.setFut(0);
            // For zoom level 0:
            final int max = Dib2Constants.UI_PAGE_WIDTH - UiValTag.UI_WIN_WIDTH.i32Fut();
            if (0 < max) {
              UiValTag.UI_PANE_OFFS_X.setFut((int) (max * vX));
            }
            return null;
          }
        }
        clicked = true;
      } else {
        if (!active) {
          if (ColorDistinct.MANGO.nmz.rgb0 != UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard]) {
            UiFrame.shiftPointerCanvas4Page(xXPx, xYPx); //xX, xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar);
            k0 = StringFunc.STEP;
            clicked = true;
          } else {
            final int eXPt = (xX - ClickRepeater.uiCPointerX0) >> Dib2Constants.UI_PT10_SHIFT;
            final int eYPt = (xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar
                - ClickRepeater.uiCPointerY0) >> Dib2Constants.UI_PT10_SHIFT;
            if ((-10 <= eXPt) && (eXPt <= 10) && (-10 <= eYPt) && (eYPt <= 10)) {
              // Use keypad ...
            } else {
              UiFrame.shiftPointerCanvas4Page(xXPx, xYPx); //xX, xY - (Dib2Constants.UI_FRAME_BARS - 1) * bar);
              return null;
            }
          }
        }
      }
      if (!clicked) {
        if (UiValTag.UI_WIN_WIDTH.i32Fut() < (UiValTag.UI_WIN_HEIGHT.i32Fut() - 4 * bar)) {
          n8Y = (8 << 3) + (((xY - (UiValTag.UI_WIN_HEIGHT.i32Fut() - bar)) * (UiValTag.qcKeys4Win + 1)) << 3)
              / UiValTag.UI_WIN_WIDTH.i32Fut();
        }
        if ((((n8Y + 5) % 8) <= 1) || (((n8X + 5) % 8) <= 1)) {
          // Too far off.
          return null;
        }
        final int iY = (((int) n8Y + 4) >> 3) - 1;
        final int iX = (((int) n8X + 4) >> 3) - 1;
        if ((0 > iY) || (iY >= UiValTag.qcKeys4Win) || (0 > iX) || (iX >= UiValTag.qcKeys4Win)) {
          return null;
        }
        k0 = UiValTag.qPadKeys[zUiKeypadInx][iY * UiValTag.qcKeys4Win + iX];
      }
    }
    if (0 == k0) {
      return null;
    }
    if ((0 >= zEntry.length())
        && (1 == zUiKeypadInx)
        && !pointerMoved
        && !clicked
        && (FeederRf.CALC == Dib2Root.app.mainFeeder)
        && (((FeederRf) (Dib2Root.app.feederCurrent)).get().getNumSlide30Supp() <= (1L << 30))) {
      // Shortcut for calculator operations.
      if ('#' == k0) {
        zEntry = BigSxg.marker4Radix(UiValTag.UI_NUMBER_BASE_SEC.i32Fut());
        ziEntry = zEntry.length();
        return null;
      } else {
        switch (k0) {
          case 'A':
            operator = QOpMain.RANGE;
            break;
          case 'B':
            operator = QOpMain.ROUND;
            break;
          case 'C':
            operator = QOpMain.POWER;
            break;
          case 'D':
            operator = QOpMain.RADD;
            break;
          case 'E':
            operator = QOpMain.E;
            break;
          case 'F':
            operator = QOpMain.PI;
            break;
          default:
            operator = Dib2Root.valueOfOr("" + k0, false, null);
        }
      }
    } else if (zUiKeypadInx >= (UiValTag.qPadKeys.length - 1)) {
      zUiKeypadInx = zUiKeypadLastInx;
      if (zUiKeypadInx >= (UiValTag.qPadKeys.length - 1)) {
        zUiKeypadInx = 2;
      }
    }
    QToken out = QToken.createTask(((' ' <= k0) || pointerMoved) ? QOpUi.zzKEY_REP : QOpUi.zzKEY);
    if (null != operator) {
      out =
          createToken4Entry(
              operator,
              true); // QScript.createTask(QOpFeed.zzAPPLY, operator, QSeq.createQSeq(t0));
    }
    out.parX = k0;
    out.parN0 = clicked ? 1 : 0;
    return out;
  }

  private QToken checkUiEventEntry(char key) {
    final int iEntry = ziEntry;
    final String entry = zEntry;
    ///// Atomic
    ziEntry = ((0 <= iEntry) && (iEntry <= entry.length())) ? iEntry : entry.length();
    if (' ' <= key) {
      zEntry = entry.substring(0, iEntry) + key + entry.substring(iEntry);
      ziEntry = iEntry + 1;
      ziButtonFocus = 0;
    }
    if (zbSlideNum) {
      double nSlide = BigSxg.doubleD4oString(zEntry, 0.0);
      if (!Double.isNaN(nSlide) && (nSlide == (int) nSlide)) {
        ((FeederRf) (Dib2Root.app.feederCurrent)).get().findSlideSupplement((int) nSlide, 1);
      }
    }
    prepareFeed();
    return null;
  }

  private QToken scrollOrCursor(char xKey) {
    int i0 = ziEntry;
    switch (xKey) {
      case MOVE_LEFT:
        ziEntry = ((0 < i0) && (i0 <= zEntry.length())) ? (i0 - 1) : 0;
        return null;
      case MOVE_UP:
        ziEntry = 0;
        return null;
      case MOVE_DOWN:
        ziEntry = zEntry.length();
        return null;
      case MOVE_RIGHT:
        ziEntry = (i0 < zEntry.length()) ? (i0 + 1) : zEntry.length();
        return null;
      case SCROLL_DOWN:
        {
          final int offs = UiValTag.UI_PANE_OFFS_Y.i32Fut();
          final int dsp =
              UiValTag.UI_WIN_HEIGHT.i32Fut()
                  - Dib2Constants.UI_FRAME_BARS * UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
          final int split = UiValTag.UI_PANE_SPLIT_Y.i32Fut();
          final int gap = UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut();
          final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut();
        final int board = Dib2Constants.UI_PAGE_HEIGHT;
          int delta = dsp >> ((0 < zoom) ? zoom : 1);
          if (((offs + gap + dsp) < board) && (split <= ((dsp >> 1) + 1))) {
            if ((0 >= offs) && ((Dib2Constants.UI_PAGE_NMZ_LF * 2) > gap)) {
              UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(Dib2Constants.UI_PAGE_NMZ_LF * 2);
            } else {
              UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(gap + delta);
            }
          } else if (split <= ((dsp >> 1) + 1)) {
            UiValTag.UI_PANE_SPLIT_Y.setFut(dsp - split);
          } else if (0 < gap) {
            delta = ((delta > split) && (split > 0)) ? split : delta;
            delta = (gap >= delta) ? delta : gap;
            UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(gap - delta);
            UiValTag.UI_PANE_OFFS_Y.setFut(offs + delta);
          } else {
            delta = ((offs + delta) < board) ? delta : (board - delta - offs);
            UiValTag.UI_PANE_OFFS_Y.setFut(offs + delta);
          }
        }
        return null;
      case SCROLL_LEFT:
        {
          final int offs = UiValTag.UI_PANE_OFFS_X.i32Fut();
          final int dsp = UiValTag.UI_WIN_WIDTH.i32Fut();
          final int split = UiValTag.UI_PANE_SPLIT_X.i32Fut();
          final int gap = UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut();
          final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut();
          int delta = dsp >> ((0 < zoom) ? zoom : 1);
          if ((0 < gap) && (split <= ((dsp >> 1) + 1))) {
            delta = (delta <= gap) ? delta : gap;
            UiValTag.UI_PANE_SPLIT_GAP_X.setFut(gap - delta);
          } else if (split <= ((dsp >> 1) + 1)) {
            UiValTag.UI_PANE_SPLIT_X.setFut(dsp - split);
          } else if (0 < offs) {
            delta = ((delta > split) && (split > 0)) ? split : delta;
            delta = (delta <= offs) ? delta : offs;
            UiValTag.UI_PANE_OFFS_X.setFut(offs - delta);
            UiValTag.UI_PANE_SPLIT_GAP_X.setFut(gap + delta);
          } else {
            UiValTag.UI_PANE_SPLIT_X.setFut(dsp - split);
          }
        }
        return null;
      case SCROLL_RIGHT:
        {
          final int offs = UiValTag.UI_PANE_OFFS_X.i32Fut();
          final int dsp = UiValTag.UI_WIN_WIDTH.i32Fut();
          final int split = UiValTag.UI_PANE_SPLIT_X.i32Fut();
          final int gap = UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut();
          final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut();
          final int board = Dib2Constants.UI_PAGE_WIDTH;
          int delta = dsp >> ((0 < zoom) ? zoom : 1);
          if (((offs + gap + dsp) < board) && (split <= ((dsp >> 1) + 1))) {
            if ((0 >= offs) && ((Dib2Constants.UI_PAGE_NMZ_TAB / 2) > gap)) {
              UiValTag.UI_PANE_SPLIT_GAP_X.setFut(Dib2Constants.UI_PAGE_NMZ_TAB / 2);
            } else {
              UiValTag.UI_PANE_SPLIT_GAP_X.setFut(gap + delta);
            }
          } else if (split <= ((dsp >> 1) + 1)) {
            UiValTag.UI_PANE_SPLIT_X.setFut(dsp - split);
          } else if (0 < gap) {
            delta = ((delta > split) && (split > 0)) ? split : delta;
            delta = (gap >= delta) ? delta : gap;
            UiValTag.UI_PANE_SPLIT_GAP_X.setFut(gap - delta);
            UiValTag.UI_PANE_OFFS_X.setFut(offs + delta);
          } else {
            delta = ((offs + delta) < board) ? delta : (board - delta - offs);
            UiValTag.UI_PANE_OFFS_X.setFut(offs + delta);
          }
        }
        return null;
      case SCROLL_UP:
        {
          final int offs = UiValTag.UI_PANE_OFFS_Y.i32Fut();
          final int dsp =
              UiValTag.UI_WIN_HEIGHT.i32Fut()
                  - Dib2Constants.UI_FRAME_BARS * UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
          final int split = UiValTag.UI_PANE_SPLIT_Y.i32Fut();
          final int gap = UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut();
          final int zoom = UiValTag.UI_ZOOMLVL_BOARD.i32Fut();
          int delta = dsp >> ((0 < zoom) ? zoom : 1);
          if ((0 < gap) && (split <= ((dsp >> 1) + 1))) {
            delta = (delta <= gap) ? delta : gap;
            UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(gap - delta);
          } else if (split <= ((dsp >> 1) + 1)) {
            UiValTag.UI_PANE_SPLIT_Y.setFut(dsp - split);
          } else if (0 < offs) {
            delta = ((delta > split) && (split > 0)) ? split : delta;
            delta = (delta <= offs) ? delta : offs;
            UiValTag.UI_PANE_OFFS_Y.setFut(offs - delta);
            UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(gap + delta);
          } else {
            UiValTag.UI_PANE_SPLIT_Y.setFut(dsp - split);
          }
        }
        return null;
      default:
        ;
    }
    return null;
  }

  private QToken checkUiEventCtrl(char xKey) {
    final int iFocus = ziButtonFocus;
    ziButtonFocus = 0;
    int i0 = ziEntry;
    QToken task = checkUiEvent(QScript.createTask4UiEvent(xKey));
    if (null == task) {
      return null;
    }
    String s0 = null;
    QEnumIf operator = null;
    QWord /*OLD*/ key = QWord /*OLD*/.createQWordInt(xKey);

    if (zbSlideNum) {
      zbSlideNum = false;
      double nSlide = BigSxg.doubleD4oString(zEntry, 0.0);
      if (!Double.isNaN(nSlide) && (nSlide == (int) nSlide)) {
        ((FeederRf) (Dib2Root.app.feederCurrent))
            .get()
            .findSlideSupplement((int) nSlide, 1); // QValMapSto.doubleD4oQVal(nSlide), 1);
      }
      return null;
    }

    switch (xKey) {

        //// Special cases first ...

      case STEP:
        operator = Dib2Root.valueOfOr(zEntry, false, null);
        if (null != operator) {
          ziEntry = 0;
          zEntry = "";
          return ((FeederRf) (Dib2Root.app.feederCurrent))
              .get()
              .tryOrFilter4Ui(QScript.createTask(operator, key, null));
        }
        // Fall through.
      case CR:
      case LF:
        if ((STEP != xKey) && (0 >= zEntry.length())) {
          QSeq arg = UiValFeedTag.peekStack();
          if ((null != arg) && (QWord.NaN != arg) && (0 < arg.size())) {
            zEntry = StringFunc.mnemonics4String(arg.toStringFull(), false);
          }
        }
        // Fall through.
      case PUSH:
      case XCOPY:
      case XCUT:
        // Keep volatile index in range:
        ziEntry = 0;
        task = createToken4Entry(null, (0 > iFocus));
        task.parN0 = xKey;
        task = ((FeederRf) (Dib2Root.app.feederCurrent)).get().tryOrFilter4Ui(task);
        if (null == task) {
          return null;
        } else if (QOpUi.zzKEY == task.op) {
          if ((StringFunc.CR == task.parN0) || (StringFunc.LF == task.parN0)) {
            zEntry = task.parS0 + "`LF.";
            ziEntry = zEntry.length();
          }
          return null;
        }
        s0 = task.parS0;
        s0 = (null == s0) ? "" : s0;
        final boolean fill = (0 >= s0.length());
        if (fill && ((0 == iFocus) || ((CR != xKey) && (LF != xKey)))) {
          QSeq arg = UiValFeedTag.peekStack();
          if ((null != arg) && (QWord.NaN != arg) && (0 < arg.size())) {
            zEntry = StringFunc.mnemonics4String(arg.toStringFull(), false);
            if (XCUT == xKey) {
              task.argX = QWord.V_1;
              operator = QOpMain.CLRN;
            } else if (STEP == xKey) {
              zEntry = "";
              operator = QOpMain.DUP;
            } else {
              // Finished.
              task = null;
            }
          }
        } else if (XCUT == xKey) {
          zEntry = "";
          task = null;
        } else if (XCOPY == xKey) {
          zEntry = s0;
          // Finished.
          task = null;
        }
        if ((XCUT == xKey) || (XCOPY == xKey)) {
          Dib2Root.platform.pushClipboard("edit", StringFunc.string4Mnemonics1345(fill ? zEntry : s0, 1L));
        }
        ziEntry = zEntry.length();
        if ((null == task) || (null == operator) || (null == task.op) || (task.op == operator)) {
          return task;
        }
        task.op = operator;
        return task;

      case PSHIFT:
        char sel = (0 >= ziEntry) ? ' ' : zEntry.charAt(ziEntry - 1);
        setUnicodeSelection(sel);
        // Fall through
      case BACKSP:
        ziEntry = ((0 < ziEntry) && (ziEntry < zEntry.length())) ? ziEntry : zEntry.length();
        if (0 < ziEntry) {
          zEntry = zEntry.substring(0, ziEntry - 1) + zEntry.substring(ziEntry);
          --ziEntry;
        }
        return null;

      case ALT:
      case SHIFT:
        if ((UiValTag.qPadKeys.length - 1) <= zUiKeypadInx) {
          if ((UiValTag.keys_UniBlock_Current <= UiValTag.keys_UniBlock_Offset) && (ALT == xKey)) {
            zUiKeypadInx = UiValTag.keys_UniBlock_FromPad;
            return null;
          }
          UiValTag.setUnicodeBlock(-1, (ALT == xKey) ? -1 : 1);
          return null;
        }
        zUiKeypadInx += (ALT != xKey) ? 1 : ((0 < zUiKeypadInx) ? -1 : 0);
        return null;
      case ESCAPE:
        ziEntry = 0;
        zEntry = "";
        return QScript.createTask(QOpMain.ESCAPE, key);

        /////

      case MOVE_LEFT:
      case MOVE_UP:
      case MOVE_DOWN:
      case MOVE_RIGHT:
      case SCROLL_LEFT:
      case SCROLL_UP:
      case SCROLL_DOWN:
      case SCROLL_RIGHT:
        if (ClickRepeater.uiCPointerMoving) {
          ClickRepeater.movePointer4Cmd(xKey);
          return null;
        }
        return scrollOrCursor(xKey);
      case TAB:
        // ...
        ziButtonFocus = (0 == iFocus) ? -1 : 0;
        return null;
      case XPASTE:
        s0 = StringFunc.mnemonics4String(Dib2Root.platform.getClipboardText(), false);
        if (null != s0) {
          final String entry = zEntry;
          final int ix = ziEntry;
          ziEntry = 0;
          if (ix <= entry.length()) {
            zEntry = entry.substring(0, ix) + s0 + entry.substring(ix);
          }
          ziEntry = zEntry.length();
        }
        return null;
      case ZOOM_IN:
      case ZOOM_OUT:
        i0 = UiValTag.UI_ZOOMLVL_BOARD.i32Fut();
        UiValTag.UI_ZOOMLVL_BOARD.setFut(i0 + ((ZOOM_IN == xKey) ? 1 : -1));
        if ((ZOOM_OUT == xKey) && (-1 >= i0)) {
          if (UiValTag.UI_PANE_SPLIT_GAP_X.i32Fut()
              < (UiValTag.UI_WIN_WIDTH.i32Fut() >> 2)) {
            UiValTag.UI_PANE_SPLIT_GAP_X.setFut(0);
          }
          if (UiValTag.UI_PANE_SPLIT_GAP_Y.i32Fut()
              < (UiValTag.UI_WIN_HEIGHT.i32Fut() >> 2)) {
            UiValTag.UI_PANE_SPLIT_GAP_Y.setFut(0);
          }
        }
        return null;
      default:
        ;
    }
    return null;
  }

  private QToken checkUiEvent(QToken cmd) {
    QToken out = null;
    if (Dib2Root.app.error instanceof ExceptionAdapter) {
      Dib2Root.ccmSto.variableForceOrIndex("E", QSeq.createQSeq(((ExceptionAdapter) Dib2Root.app.error).toString()));
    }
    Dib2Root.app.error = null;
    if (null == cmd) {
      // NOP
    } else if ((QOpMain.CLEAR == cmd.op) && (0 < zEntry.length())) {
      ziEntry = 0;
      zEntry = "";
    } else if (!(cmd.op instanceof QOpUi)) {
      if ("ESCAPE".equals(cmd.op.name())) {
        UiValTag.resetAll();
        ((FeederRf) (Dib2Root.app.feederCurrent)).get().findSlideSupplement(1, 1L);
        ((FeederRf) (Dib2Root.app.feederCurrent)).get().start();
      }
      cmd = ((FeederRf) (Dib2Root.app.feederCurrent)).get().tryOrFilter4Ui(cmd);
      if (null != cmd) {
        return cmd;
      }
    } else {
      QSeq arg = (cmd.argX instanceof QSeq) ? (QSeq) cmd.argX : null;
      switch ((QOpUi) cmd.op) {
        case BAS2:
          if ((null == arg) || arg.atom().isNumeric()) {
            int v0 = (null == arg) ? 0 : (int) arg.atom().i64();
            v0 = ((10 == v0) || (12 == v0) || (16 == v0) || (60 == v0)) ? v0 : 0;
            if (0 == v0) {
              v0 = UiValTag.UI_NUMBER_BASE_SEC.i32Fut();
              v0 = (10 == v0) ? 12 : ((12 == v0) ? 16 : ((16 == v0) ? 60 : 10));
            }
            UiValTag.UI_NUMBER_BASE_SEC.setFut(v0);
            if (UiValTag.kBarTools[2].startsWith("BS")) {
              UiValTag.kBarTools[2] = "BS" + v0;
            }
          }
          break;
        case LANG:
          if ((null == arg) || (0 >= arg.size()) || arg.atom().isNumeric()) {
            final int e0 = ((null == arg) || (0 >= arg.size())) ? 1 : (int) arg.atom().i64();
            // UiValTag.UI_LANGUAGE.setFut((Dib2Root.ui.iLang + e0) % Dib2Lang.kLanguages.length);
            Dib2Root.ui.iLang = (Dib2Root.ui.iLang + e0) % Dib2Lang.kLanguages.length;
          } else {
            final String str = arg.toStringFull().toUpperCase(Locale.ROOT);
            for (int i0 = Dib2Lang.kLanguages.length - 1; i0 >= 0; --i0) {
              if (str.startsWith(Dib2Lang.kLanguages[i0])) {
                Dib2Root.ui.iLang = i0;
              }
            }
          }
          UiValTag.kBarTools[0] = "LA" + Dib2Lang.kLanguages[Dib2Root.ui.iLang];
          break;
        case UICOD:
          if (null != arg) {
            final String str = arg.toStringFull();
            if ((null != str) && (0 < str.length())) {
              UiValTag.setUnicodeBlockOffset(str);
            }
          }
          break;
        case ABOUT:
          Dib2Root.app.feederNext = FeederRf.ABOUT;
          break;
        case HELP:
          Dib2Root.app.feederNext = FeederRf.HELP;
          break;
        case VIEW:
          if (Dib2Root.app.appState.ordinal() > Dib2Lang.AppState.DISCLAIMER.ordinal()) {
            if ((null == arg) || (0 >= arg.size())) {
              switch ((FeederRf) Dib2Root.app.feederCurrent) {
                case ABOUT:
                case HELP:
                  Dib2Root.app.feederNext = Dib2Root.app.mainFeeder;
                  break;
                case LICENSE:
                  Dib2Root.app.feederNext = FeederRf.HELP;
                  break;
                case CHAT:
                  Dib2Root.app.feederNext = FeederRf.LICENSE;
                  break;
                default:
                  Dib2Root.app.feederNext = ((null == QOpNet.messenger) || (null == Dib2Root.ccmSto.hidden_get("smtp_user")))
                          ? FeederRf.LICENSE
                          : FeederRf.CHAT;
              }
            } else {
              FeederRf fd = FeederRf.findFeeder(arg.toStringFull());
              Dib2Root.app.feederNext = (null == fd) ? Dib2Root.app.feederNext : fd;
            }
          } else {
            //Dib2Root.app.appState = Dib2Lang.AppState.ACTIVE;
            Dib2Root.app.feederNext = FeederRf.LICENSE;
          }
          break;
        default:
          cmd = ((FeederRf) (Dib2Root.app.feederCurrent)).get().tryOrFilter4Ui(cmd);
          out = cmd;
      }
    }
    ClickRepeater.stopMouseRep();
    prepareFeed();
    return out;
  }

  public void handleKey(char k, boolean asLongClick) {
    if (' ' <= k) {
      checkUiEventEntry(k);
    } else {
      if (asLongClick) {
        if (ESCAPE == k) {
          if (Dib2Lang.AppState.ACTIVE.ordinal() == Dib2Root.app.appState.ordinal()) {
            Dib2Root.app.appState = Dib2Lang.AppState.EXIT_REQUEST;
          }
          Dib2Root.platform.invalidate();
          return;
        }
      }
      QToken cmd = checkUiEventCtrl(k);
      cmd =
          (null == cmd)
              ? null
              : ((FeederRf) (Dib2Root.app.feederCurrent)).get().tryOrFilter4Ui(cmd);
      if (null != cmd) {
        Dib2Root.schedulerTrigger.trigger(cmd);
        return;
      }
    }
    prepareFeed();
  }

  public boolean handleMouse(int xXPx, int xYPx) {
    QToken cmd = checkUiEventMouse(xXPx, xYPx);
    if (null != cmd) {
      cmd = checkUiEvent(cmd);
    }
    if (null == cmd) {
      prepareFeed();
      return false;
    }
    if ((QOpUi.zzKEY_REP == cmd.op) && (0 < cmd.parX)) {
      return Dib2Root.schedulerTrigger.triggerExt(ClickRepeater.INSTANCE, cmd);
    }
    ClickRepeater.stopMouseRep();
    int keyOrButton;
    if ((null == cmd) || (null == cmd.op)) {
      prepareFeed();
      return false;
    } else if ((QOpUi.zzKEY == cmd.op) && (0 < (keyOrButton = cmd.parX))) {
      handleKey((char) keyOrButton, false);
      return true;
    }
    Dib2Root.schedulerTrigger.trigger(cmd);
    return false;
  }

  private QScript[] barTitle_script = new QScript[30];

  private QScript barTitle() {
    int no = 0;
    int count = 0;
    final int need = 1 + UiValTag.kBarTitle.length * 3 / 2;
    if (need > barTitle_script.length) {
      barTitle_script = new QScript[need];
    }
    final int e0 = UiValFeedTag.UI_LINE_SPACING_PT10.i32(UiValTag.getTick());
    int posX = UiValTag.UI_WIN_WIDTH.i32Fut() - Dib2Constants.UI_WIN_MARGIN;
    final int base =
        (UiValTag.UI_BAR_PANE_HEIGHT.i32Fut()
                + ((UiValTag.UI_FONT_SIZE_WIN_PT10.i32Fut() * Dib2Constants.UI_FONT_NMZ_CAP_H)
                    >> Dib2Constants.UI_FONT_NMZ_SHIFT))
            >> 1;
    int step = -2;
    barTitle_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base);
    for (int i0 = UiValTag.kBarTitle.length - 2;
        (i0 != (2 * Dib2Constants.UI_FRAME_BAR_ITEMS_PER_SIDE)) || (0 > step);
        i0 += step) {
      barTitle_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, posX, 0);
      posX += (0 > step) ? (-e0) : e0;
      final int color =
          ((i0 == UiValTag.kBarTitle_qiSwitchKeyboard) && ClickRepeater.uiCPointerMoving)
              ? ColorDistinct.AMARANTH.argbQ(0)
              : ColorNmz.findDistinct(UiValTag.kBarTitle[i0]).argbQ(0);
      barTitle_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, color);
      char ch = (char) UiValTag.kBarTitle[i0 + 1];
      barTitle_script[count++] =
          QScript.makeScriptEl(
              ++no,
              (0 > step) ? QOpGraph.TXSHLEFT : QOpGraph.TEXT,
              "" + ((' ' <= ch) ? ch : StringFunc.kControlAsButton[ch]));
      if (i0 == (2 * Dib2Constants.UI_FRAME_BAR_ITEMS_PER_SIDE)) {
        step = 2;
        posX = Dib2Constants.UI_WIN_MARGIN;
        i0 = -2;
      }
    }
    QScript out = QScript.makeScript(0);
    out.script = barTitle_script;
    out.cScript = count;
    return out;
  }

  private QScript[] barTools_script = new QScript[50];

  private QScript barTools() {
    int no = 0;
    int count = 0;
    final int need = 3 + 2 * 2 * UiValTag.kBarTools.length;
    if (need > barTools_script.length) {
      barTools_script = new QScript[need];
    }
    int posX = Dib2Constants.UI_WIN_MARGIN;
    final int e0 = UiValTag.UI_BAR_PANE_HEIGHT.i32Fut();
    final int linesp = (3 * e0) >> 3;
    barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.FACE, 0);
    barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.HEIGHT, linesp);
    final int base =
        (e0
                - (linesp
                    - ((linesp * Dib2Constants.UI_FONT_NMZ_CAP_H)
                        >> Dib2Constants.UI_FONT_NMZ_SHIFT)))
            >> 1;
    barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base);
    for (int i0 = 0; i0 < UiValTag.kBarTools.length; ++i0, posX += e0) {
      String part0 = kBarTools[i0].substring(0, 2);
      String part1 = kBarTools[i0].substring(2);
      if ((0 > ziButtonFocus) && "GO".equals(part1)) {
        // ...
        part0 = "**";
      } else if ("VW".equals(part0)) {
        part1 = ((FeederRf) (Dib2Root.app.feederCurrent)).getShortId2();
      }
      barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, posX, 0);
      barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TEXT, part0);
      barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, posX, linesp);
      barTools_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TEXT, part1);
    }
    QScript out = QScript.makeScript(0);
    out.script = barTools_script;
    out.cScript = count;
    return out;
  }

  private QScript[] barEntry_script = new QScript[10];

  private QScript barEntry() {
    int no = 0;
    int count = 0;
    // Atomic!
    int iEntry = ziEntry;
    String entry = zEntry;
    iEntry = (0 <= iEntry) && (iEntry <= entry.length()) ? iEntry : entry.length();
    final int e0 = (5 * UiValTag.UI_WIN_WIDTH.i32Fut()) >> 3;
    final String part0 = entry.substring(0, iEntry);
    final String part1 = entry.substring(iEntry);
    final int posX = UiValTag.UI_WIN_WIDTH.i32Fut() - Dib2Constants.UI_WIN_MARGIN;
    int base =
        (UiValTag.UI_BAR_PANE_HEIGHT.i32Fut()
                + ((UiValTag.UI_FONT_SIZE_WIN_PT10.i32Fut() * Dib2Constants.UI_FONT_NMZ_X_HEIGHT)
                    >> Dib2Constants.UI_FONT_NMZ_SHIFT))
            >> 1;
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, e0, 0);
    barEntry_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.LINE, e0, UiValTag.UI_BAR_PANE_HEIGHT.i32Fut());
    barEntry_script[count++] =
        QScript.makeScriptEl(
            ++no,
            QOpGraph.POS,
            e0
                - (part0.endsWith(" ")
                    ? (Dib2Constants.UI_WIN_MARGIN << 1)
                    : Dib2Constants.UI_WIN_MARGIN),
            0);
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base);
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXSHLEFT, part0);
    barEntry_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.POS, e0 + Dib2Constants.UI_WIN_MARGIN, 0);
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TEXT, part1);
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, posX, 0);
    barEntry_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, ColorDistinct.APPLE_GREEN.argbQ(0));
    barEntry_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXSHLEFT, ">");
    QScript out = QScript.makeScript(0);
    out.script = barEntry_script;
    out.cScript = count;
    return out;
  }

  private QScript[] barStatus_script = new QScript[13];

  private QScript barStatus() {
    int no = 0;
    int count = 0;
    final int height = (3 * UiValTag.UI_BAR_PANE_HEIGHT.i32Fut() >> 3);
    final int posX = UiValTag.UI_WIN_WIDTH.i32Fut() - Dib2Constants.UI_WIN_MARGIN;
    String progress = Dib2Root.ui.progress;
    if (ClickRepeater.uiCPointerMoving || ((0 <= ClickRepeater.uiCPointerX0) && (0 <= ClickRepeater.uiCPointerY0)
        && (ColorDistinct.PURE_RED.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard]))) {
      progress = ("" + (ClickRepeater.uiCPointerX0 >> (Dib2Constants.UI_PT10_SHIFT - 1)) + ' '
          + (ClickRepeater.uiCPointerY0 >> (Dib2Constants.UI_PT10_SHIFT - 1)));
    }
    if (null != Dib2Root.app.error) {
      progress = ((ExceptionAdapter) Dib2Root.app.error).toString();
    }
    int prcolor = ColorDistinct.PURPLISH_PINK.argbQ(0);
    if (MainThreads.isIdle() && (null == Dib2Root.app.error)) {
      Dib2Root.ui.progress = null;
      FeederRf fx =
          (Dib2Root.app.feederCurrent instanceof FeederRf)
              ? (FeederRf) Dib2Root.app.feederCurrent
              : null;
      if (0 < Dib2Root.app.alarmTime_msec) {
        progress = DateFunc.dateShort4Millis(Dib2Root.app.alarmTime_msec).substring(7, 12);
        prcolor = ColorDistinct.AMARANTH.argbQ(0);
      } else {
        progress =
            ((fx == null)
                    || (Dib2Lang.AppState.ACTIVE != Dib2Root.app.appState)
                    || !fx.name().contains("INTRO"))
                ? Dib2Root.app.appState.transls[Dib2Root.ui.iLang]
                : "INTRO";
        // (0 != (4 & Dib2Root.app.bDone_CreateConfigInit)) ? "OK" : "INIT";
        prcolor = ColorDistinct.APPLE_GREEN.argbQ(0);
      }
    }
    final int base =
        (UiValTag.UI_BAR_PANE_HEIGHT.i32Fut()
                + ((height // UiDataSto.UI_FONT_SIZE_FRAME_PT10.i32( null)
                        * Dib2Constants.UI_FONT_NMZ_CAP_H)
                    >> Dib2Constants.UI_FONT_NMZ_SHIFT))
            >> 1;
    final int eBase4TwoLines =
        ((null != progress) && (10 <= progress.length())) ? (height >> 1) : 0;
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, 0, 0);
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base - eBase4TwoLines);
    barStatus_script[count++] =
        QScript.makeScriptEl(
            ++no, QOpGraph.HEIGHT, height); // UiDataSto.UI_DISPLAY_BAR_HEIGHT.i32( null) >> 1);
    barStatus_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.POS, UiValTag.UI_WIN_WIDTH.i32Fut() >> 1, 0);
    barStatus_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, ColorNmz.ColorDistinct.FG__BLACK.argbQ(1));
    final long nSlide30 =
        (null == Dib2Root.app.feederCurrent)
            ? (1 << 30)
            : ((FeederRf) (Dib2Root.app.feederCurrent)).get().getNumSlide30Supp();
    final int nPageOffset = (null == Dib2Root.app.feederCurrent) ? 0
        : ((FeederRf) (Dib2Root.app.feederCurrent)).get().getPageOffset();
    final String nSlide =
        zbSlideNum
            ? "*"
            : (""
                + ((nSlide30 >>> 30) - nPageOffset)
                + ((0 != (nSlide30 & 0x3fffffff))
                    ? ("" + (char) ('a' - 1 + (nSlide30 & 0xfff)))
                    : ""));
    String textCenter =
        "<<  < "
            + nSlide
            + ((null == Dib2Root.app.feederCurrent)
                ? ""
                : ("/ " + (((FeederRf) (Dib2Root.app.feederCurrent)).get().getCountSlides() - nPageOffset)))
            + " >  >>";
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, textCenter);
    if (null == progress) {
      progress = "...";
    }
    barStatus_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.POS, Dib2Root.UI_WIN_MARGIN, eBase4TwoLines);
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, prcolor);
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TEXT, progress);
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, posX, 0);
    barStatus_script[count++] =
        QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, ColorDistinct.APPLE_GREEN.argbQ(0));
    barStatus_script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXSHLEFT, ">");
    QScript out = QScript.makeScript(0);
    out.script = barStatus_script;
    out.cScript = count;
    return out;
  }

  QScript keypad() {
    int no = 0;
    int count = 0;
    boolean selective =
        ColorDistinct.APPLE_GREEN.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard];
    int second = (ColorDistinct.SEA.nmz.rgb0 == UiValTag.kBarTitle[UiValTag.kBarTitle_qiSwitchKeyboard])
        ? ColorDistinct.SEA.argbQ(1)
        : ColorDistinct.MANGO.argbQ(-1);
    QScript[] script = new QScript[2 + 10 + 3 * UiValTag.qcKeys4Win * UiValTag.qcKeys4Win];
    final int ex = UiValTag.UI_WIN_WIDTH.i32Fut() / (UiValTag.qcKeys4Win + 1);
    int maxHeight = (UiValTag.UI_WIN_HEIGHT.i32Fut() - 4 * UiValTag.UI_BAR_PANE_HEIGHT.i32Fut());
    final int offsY = (UiValTag.UI_WIN_WIDTH.i32Fut() < maxHeight)
        ? (maxHeight - UiValTag.UI_WIN_WIDTH.i32Fut())
        : 0;
    maxHeight -= offsY;
    final int linesp = maxHeight / (UiValTag.qcKeys4Win + 1);
    final int height = (3 * linesp) >> 2;
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.HEIGHT, height);
    final int base =
        linesp
            - ((linesp
                    - ((height * Dib2Constants.UI_FONT_NMZ_CAP_H)
                        >> Dib2Constants.UI_FONT_NMZ_SHIFT))
                >> 1);
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.RBASE, base);
    int color = selective ? (ColorDistinct.ULTRAMARINE.argbQ(-1)) : second;
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, color);
    int nY = (linesp >> 1) + offsY;
    char tK = StringFunc.kControlAsButton[StringFunc.SCROLL_UP];
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, ex >> 2, 0);
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, "" + tK);
    tK = StringFunc.kControlAsButton[StringFunc.SCROLL_RIGHT];
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, ex * UiValTag.qcKeys4Win + (ex >> 1), 0);
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, "" + tK);
    final int eCharBox = linesp >> 2;
    for (int i1 = 0; i1 < UiValTag.qcKeys4Win; ++i1) {
      int nX = ex; // (ex + UiValTag.qcKeys4Win) >> 1;
      for (int i0 = 0; i0 < UiValTag.qcKeys4Win; ++i0) {
        tK = UiValTag.qPadKeys[zUiKeypadInx][i1 * UiValTag.qcKeys4Win + i0];
        script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, nX, nY - eCharBox);
        color = selective ? (ColorDistinct.APPLE_GREEN.argbQ(1)) : second;
        if (' ' > tK) {
          tK = StringFunc.kControlAsButton[tK];
          color = selective ? (ColorDistinct.ULTRAMARINE.argbQ(1)) : second;
        }
        script[count++] = QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, color);
        script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, "" + tK);
        nX += ex;
      }
      nY += linesp;
    }
    nY -= (linesp >> 1);
    color = selective ? (ColorDistinct.ULTRAMARINE.argbQ(-1)) : second;
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.RGBCOLOR, color);
    tK = StringFunc.kControlAsButton[StringFunc.SCROLL_LEFT];
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, ex >> 2, nY);
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, "" + tK);
    tK = StringFunc.kControlAsButton[StringFunc.SCROLL_DOWN];
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.POS, ex * UiValTag.qcKeys4Win + (ex >> 1), nY);
    script[count++] = QScript.makeScriptEl(++no, QOpGraph.TXCTR, "" + tK);
    QScript out = QScript.makeScript(0);
    out.script = script;
    out.cScript = count;
    return out;
  }

  private void setUnicodeSelection(char base) {
    UiValTag.keys_UniBlock_Offset = UiValTag.setUnicodeBlock(base, 0);
    UiValTag.keys_UniBlock_FromPad = zUiKeypadInx;
    String group = StringFunc.group4Rfc1345(base);
    int count = 0;
    final int len =
        (group.length() > UiValTag.keys_UnicodeSel.length)
            ? UiValTag.keys_UnicodeSel.length
            : group.length();
    for (int i0 = 0; i0 < len; ++i0) {
      if (UiValTag.kKeys_A_Dvorak[i0] <= ' ') {
        UiValTag.keys_UnicodeSel[i0] = UiValTag.kKeys_A_Dvorak[i0];
      } else {
        UiValTag.keys_UnicodeSel[i0] = group.charAt(count++);
      }
    }
    zUiKeypadLastInx = zUiKeypadInx;
    zUiKeypadInx = UiValTag.qPadKeys.length - 1;
  }

  /** Create title bar, status bar etc. */
  static synchronized void prepareUiFrameData() {
    QToken token;
    while (null != (token = INSTANCE.wxGateIn4Feed.pull())) {
      if (token.op instanceof QOpUi) {
        switch ((QOpUi) token.op) {
          case zzENTRY:
            INSTANCE.ziEntry = 0;
            INSTANCE.zEntry = StringFunc.mnemonics4String(
                (token.argX instanceof QSeq) ? ((QSeq) token.argX).toStringFull() : token.argX.toString(), false);
            INSTANCE.ziEntry = INSTANCE.zEntry.length();
            break;
          case LANG:
          case UICOD:
          case VIEW:
            INSTANCE.checkUiEvent(token);
            break;
            //
          case zzSET:
            ((UiValTag) token.argX).setFut(token.parX);
            break;
          default:
            ;
        }
      }
    }
    // For recycling - to be done after atomic value replacements.
    QScript[] oldTitles =
        new QScript[] {qUiBarTitle, qUiBarTools, qUiBarEntry, qUiBarStatus, qUiKeypad};
    qUiBarTitle = INSTANCE.barTitle();
    qUiBarTools = INSTANCE.barTools();
    qUiBarEntry = INSTANCE.barEntry();
    qUiBarStatus = INSTANCE.barStatus();
    qUiKeypad = INSTANCE.keypad();
    for (QScript scr : oldTitles) {
      if (null != scr) {
        scr.recycleMe();
      }
    }
  }

  // =====
}
