// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_wk;

import com.gitlab.dibdib.picked.common.ExceptionAdapter;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_io.*;
import net.sf.dibdib.thread_net.QOpNet;
import net.sf.dibdib.thread_wk.CcmSto.CcmTag;

public enum QOpWk implements QIfs.QEnumIf {
  zzARCHIVE,
  zzDATA,
  zzSAV1,
  zzSAV1_zzEXIT,
  zzQSTO,
  zzQUP,
  zzIMPORT,
//  zzEXEC,
;

  public static QIfs.QEnumIf[] create() {
    return values();
  }

  @Override
  public long getShash() {
    return ShashFunc.shashBits4Ansi(name());
  }

  private static void setHost(String host, String user, String imapPort, String smtpPort) {
    Dib2Root.ccmSto.hidden_set("smtp_server", StringFunc.bytesUtf8("smtp." + host));
    Dib2Root.ccmSto.hidden_set("smtp_user", StringFunc.bytesUtf8(user));
    Dib2Root.ccmSto.hidden_set(
        "smtp_port", StringFunc.bytesUtf8((null != smtpPort) ? smtpPort : "587"));
    Dib2Root.ccmSto.hidden_set("imap_server", StringFunc.bytesUtf8("imap." + host));
    Dib2Root.ccmSto.hidden_set("imap_user", StringFunc.bytesUtf8(user));
    Dib2Root.ccmSto.hidden_set(
        "imap_port", StringFunc.bytesUtf8((null != imapPort) ? imapPort : "993"));
  }

  private static void setHost(String[] a0) {
    final String host = a0[0];
    final String user = a0[1];
    final String imapPort = (2 >= a0.length) ? null : a0[2];
    final String smtpPort = (3 >= a0.length) ? null : a0[3];
    setHost(host, user, imapPort, smtpPort);
  }

  private static HashSet<QWordIf> findMsgsToAckOrResend(long chat, long bAckResend) {
    HashSet<Long> msgs = CcmSto.findMsgs4Chat(chat);
    HashSet<QWordIf> msgs2 = new HashSet<QWordIf>(msgs.size());
    for (Long msg : msgs) {
      final QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.search(msg);
      if ((item instanceof QSTuple) && (null != CcmTag.RECV.getValue((QSTuple) item))) {
        String ack = ((QSeq) (CcmTag.RECV.getValue((QSTuple) item))).toStringFull();
        final int iAckTime = ack.indexOf("*:") + 2;
        if ((2 <= iAckTime) && ((iAckTime + 5) < ack.length())
            && (ack.substring(iAckTime, iAckTime + 5).trim().matches("[1-9][0-9]+"))) {
          long ctrb = CcmTag.CNTRB.getWord((QSTuple) item).i64();
          if (((ctrb == 0) && (0 != (bAckResend & 2)))
              || ((ctrb != 0) && (0 != (bAckResend & 1)))) {
            msgs2.add(QWord.createQWordInt(msg));
          }
        }
      }
    }
    return msgs2;
  }

  public static QSeq execOpMain(QOpMain op, QSeq[] args) {
    QSeq out0 = null;
    try {
      QIfs.QItemIf item = null;
      String str;
      long bits;
      boolean ok = true;
      switch (op) {
          //  case ABOUT:
          //          //        Dib2Root.app.feederCurrent = FeederRf.ABOUT.get().onStart();
          //          return null;
        case EXIT:
          // Dib2Root.app.exitRequestTraced = ExceptionAdapter.EXIT; // ...
          Dib2Root.app.appState = AppState.EXIT_REQUEST;
          // feedProgress( new String[] { "Program is about to stop ... (Please press ENTER)." } );
          //          MainThreads.INSTANCE.triggerSaveOrInitialLoad();
          // Dib2Root.schedulerTrigger.trigger(QToken.createTask(QOpFeed.zzSAV_zzEXIT));
          return null;
          //  case HELP:
          //          //        Dib2Root.app.feederCurrent = FeederRf.HELP.get().onStart();
          //          return null;
        case INIT:
        case PW:
        case PWAC:
        case QME:
          str = args[0].toStringFull();
          if ((null == str) || (0 >= str.length())) {
            return null;
          }
          if (QOpMain.PW == op) {
            ok = TcvCodec.instance.settleHexPhrase(StringFunc.hexUtf8(str, false));
          } else if (QOpMain.PWAC == op) {
            ok = TcvCodec.instance.setAccessCode(str.getBytes(StringFunc.CHAR16UTF8));
          } else if ((QOpMain.INIT == op) || (0 < str.indexOf('@'))) {
            if (QOpMain.INIT == op) {
              String[] a0 = str.split(" ");
              if (3 > a0.length) {
                out0 = QWord.FALSE;
                break;
              } else if (3 < a0.length) {
                setHost(Arrays.copyOfRange(a0, 3, a0.length));
              }
              ok = TcvCodec.instance.setAccessCode(a0[0].getBytes(StringFunc.CHAR16UTF8));
              ok = ok && TcvCodec.instance.settleHexPhrase(StringFunc.hexUtf8(a0[2], false));
              str = a0[1];
            }
            boolean changed = !str.equals(Dib2Root.ccmSto.mUserAddr);
            Dib2Root.ccmSto.mUserAddr = str;
            ok = ok && (0 < str.indexOf('@'));
            if (ok) {
              byte[] old = Dib2Root.ccmSto.hidden_get("email_address");
              if (null != old) {
                Dib2Root.ccmSto.hidden_set(
                    "email_address", StringFunc.bytesUtf8(Dib2Root.ccmSto.mUserAddr));
              }
              if (changed && (null != Dib2Root.ccmSto.hidden_get("smtp_user"))) {
                return QSeq.createQSeq(QOpNet.zzCLR);
              }
            }
          }
          out0 = ok ? QWord.V_DOT : QWord.FALSE;
          break;
        case MMC:
          str = args[0].toStringFull();
          if ((null != args) && (null != str) && (0 < str.length())) {
            Dib2Root.ccmSto.variable_set(str, null);
            if (1 == str.length()) {
              str = "M" + str.charAt(0);
              Dib2Root.ccmSto.variable_set(str, null);
            }
          }
          return null;
        case MMCA:
          Dib2Root.ccmSto.clearXVar(true);
          return null;
        case MMLD:
          str = args[0].toStringFull();
          if (1 == str.length()) {
            final char ch = str.charAt(0);
            switch (ch) {
              case '!':
                str = "T";
                break;
              default:
                str = "M" + ch;
            }
          }
          out0 = Dib2Root.ccmSto.variable_get(str);
          break;
        case MMSTO:
          str = args[1].toStringFull();
          if (1 == str.length()) {
            str = "M" + str.charAt(0);
          }
          Dib2Root.ccmSto.variable_set(str, args[0]);
          out0 = args[0];
          break;
        case QDEL:
          item = args[0].at(0);
          bits = (item instanceof QWord) ? ((QWord) item).i64() : 0;
          item = Dib2Root.ccmSto.zMappingsPid.search(bits);
          out0 = QWord.NaN;
          if (item instanceof QSTuple) {
            Dib2Root.ccmSto.zMappingsPid.remove(bits, item);
            bits = ((QSTuple) item).getAsKey(CcmTag.LABEL);
            Dib2Root.ccmSto.zMappingsLabel.remove(bits, item);
            item = ((QSTuple) item).getValue(CcmTag.LABEL);
            out0 = (item instanceof QSeq) ? ((QSeq) item) : QWord.FALSE;
          }
          break;
        case QDFC:
          str = args[0].toStringFull().trim();
          bits = (1 >= str.length()) ? 0 : Cats.toFlags(str);
          if (0 == bits) {
            Dib2Root.ccmSto.variable_remove("C");
          } else {
            str = Cats.cats4Flags(bits);
            Dib2Root.ccmSto.variableForceOrIndex("C", QSeq.createQSeq(str));
          }
          out0 = QWord.createQWordInt(bits);
          break;
        case QHOST:
          str = args[0].toStringFull();
          setHost(str.split(" "));
          return null;
        case QID:
          item = Dib2Root.ccmSto.variable_get("C");
          bits = (item instanceof QWord) ? Cats.toFlags((QWord) item) : 0;
          bits = (0 != bits) ? bits : Cats.DEFAULT.flag;
          item = args[0];
          out0 = QWord.NaN;
          if (item instanceof QSeq) {
            QItemIf[] list =
                CcmSto.searchBunch(
                    (QSeq) item,
                    bits); // Dib2Root.ccmSto.zMappingsLabel.searchBunch(item.getShash());
            if (null != list) {
              QWord[] ox = new QWord[list.length];
              int i0 = 0;
              for (; i0 < list.length; ++i0) {
                ox[i0] = QWord.createQWordInt(list[i0].getShash());
              }
              if (1 == i0) {
                out0 = ox[0];
              } else if (1 < i0) {
                out0 = QSeq.createQSeq(Arrays.copyOf(ox, i0));
              }
            }
          }
          break;
        case QLOAD:
          out0 = QWord.NaN;
          item = args[0].at(0);
          bits = (item instanceof QWord) ? ((QWord) item).i64() : 0;
          item = Dib2Root.ccmSto.zMappingsPid.search(bits);
          if (item instanceof QSTuple) {
            item = ((QSTuple) item).getValue(CcmTag.DAT);
            if (item instanceof QSeq) {
              out0 = (QSeq) item;
            }
          }
          break;
        case QMINVIT:
        case QMACK:
          if (args[0] instanceof QSeq) {
            long pid = CcmSto.peek((QSeq) args[0], Cats.CHAT.flag, true);
            if (0 != pid) {
              long pid2 = CcmSto.ensureContact4Chat(pid, (QOpMain.QMINVIT != op), (QOpMain.QMINVIT == op));
              if (0 == pid2) {
                break;
              } else if (pid != pid2) {
                // Need key exchange first:
                QToken t0 = QToken.createTask((QOpMain.QMINVIT != op) ? QOpNet.zzACK : QOpNet.INVIT,
                    QWord.createQWordInt(pid));
                Dib2Root.schedulerTrigger.trigger(t0);
                break;
              }
              if (QOpMain.QMINVIT != op) {
                HashSet<QWordIf> msgs2 = findMsgsToAckOrResend(pid, 1L);
                if (0 < msgs2.size()) {
                  QToken t0 =
                      QToken.createTask(
                          QOpNet.zzSEND,
                          QSeq.createQSeq((QWordIf[]) msgs2.toArray(new QWordIf[0])),
                          QWord.createQWordInt(pid));
                  Dib2Root.schedulerTrigger.trigger(t0);
                }
              }
            }
          }
          break;
        case QMSEND:
          if ((args[0] instanceof QSeq) && (args[1] instanceof QSeq)) {
            str = null;
            long hMsg = 0;
            QSeq to = (QSeq) args[1];
            item = null;
            if (args[0] instanceof QWord) {
              hMsg = ((QWord) args[0]).i64();
              if (0 != hMsg) {
                item = Dib2Root.ccmSto.zMappingsPid.search(hMsg);
                if (null != item) {
                  str = ((QSeq) (((QSTuple) item).getValue(CcmTag.DAT))).toStringFull();
                  if (".".equals(to.toString())) {
                    to = CcmTag.TAGSREFS.getWord((QSTuple) item);
                    if (null == to) {
                      break;
                    }
                  }
                }
              }
            }
            long pid = CcmSto.peek(to, Cats.CHAT.flag, true);
            if (0 == pid) {
              break;
            }
            item = Dib2Root.ccmSto.zMappingsPid.search(pid);
            if (null == str) {
              str = StringFunc.string4Mnemonics1345(((QSeq) args[0]).toStringFull(), 0L);
              QSTuple entry = CcmSto.createEntry(QWord.V_0, Cats.MSG.flag, QSeq.createQSeq(str));
              QWord label =
                  QWord.createQWord(
                      CcmTag.LABEL.getValue((QSTuple) item).toString()
                          + ":"
                          + BigSxg.sxg4Long(entry.stamp),
                      true);
              entry.set(CcmTag.LABEL, label);
              entry.set(CcmTag.RECV, QSeq.createQSeq("*:101000"));
              item = entry;
              Dib2Root.ccmSto.zMappingsPid.add4Handle(entry);
              Dib2Root.ccmSto.zMappingsLabel.add4Multi(label.shash, entry);
            }

            if (null != item) {
              HashSet<QWordIf> msgs2 = findMsgsToAckOrResend(pid, 3L);
              //msgs2.add(QWord.createQWordInt(item.getShash()));
              QToken t0 =
                  QToken.createTask(
                      QOpNet.zzSEND,
                      QSeq.createQSeq((QWordIf[]) msgs2.toArray(new QWordIf[0])),
                      QWord.createQWordInt(pid));
              Dib2Root.schedulerTrigger.trigger(t0);
            }
          }
          break;
        case QS:
          item = Dib2Root.ccmSto.variable_get("C");
          bits = (item instanceof QWord) ? Cats.toFlags((QWord) item) : 0;
          bits = (0 != bits) ? bits : Cats.DEFAULT.flag;
          item = args[1].at(0);
          out0 = QWord.FALSE;
          if ((item instanceof QWord) && (0 != ((QWord) item).shash)) {
            final String dat = StringFunc.string4Mnemonics1345(
                (args[0] instanceof QSeq) ? ((QSeq) args[0]).toStringFull() : args[0].toString(), 0L);
            QSTuple entry = CcmSto.createEntry((QWord) item, bits, QSeq.createQSeq(dat));
            Dib2Root.ccmSto.zMappingsPid.add4Handle(entry);
            Dib2Root.ccmSto.zMappingsLabel.add4Multi(((QWord) item).shash, entry);
            out0 = QWord.createQWordInt(entry.getShash());
          }
          break;
        case QUP:
        case QUPTAGGED:
          item = args[0].at(0);
          out0 = QWord.NaN;
          if (item instanceof QWord) {
            long pid = ((QWord) item).i64();
            //out0 = ((QWord) item);
            item = Dib2Root.ccmSto.zMappingsPid.search(pid);
            if (item instanceof QSTuple) {
              if (QOpMain.QUPTAGGED != op) {
                ((QSTuple) item).set(CcmTag.DAT, args[1]);
                out0 = ((QWord) item);
              } else if (null != args[2]) {
                QItemIf dat = ((QSTuple) item).getValue(CcmTag.DAT);
                if (dat instanceof QSeq) {
                  String dat2 = ((QSeq) dat).toStringFull();
                  dat2 = CcmSto.setTaggedValue(args[1].toStringFull(), dat2, args[2].toStringFull());
                  ((QSTuple) item).set(CcmTag.DAT, QSeq.createQSeq(dat2));
                  out0 = ((QWord) item);
                }
              }
              //} else {
              //out0 = QWord.NaN;
            }
          }
          break;
        case QUPCAT:
          bits = Cats.toFlags(args[1].toStringFull());
          item = args[0].at(0);
          out0 = QWord.NaN;
          if ((0 != bits) && (item instanceof QWord)) {
            long pid = ((QWord) item).i64();
            out0 = ((QWord) item);
            item = Dib2Root.ccmSto.zMappingsPid.search(pid);
            if (item instanceof QSTuple) {
              ((QSTuple) item).set(CcmTag.CATS, QWord.createQWordInt(bits));
            } else {
              out0 = QWord.NaN;
            }
          }
          break;
        case RCQ:
          out0 = Dib2Root.ccmSto.variable_get("Q");
          break;
        case RCL:
          out0 = Dib2Root.ccmSto.variable_get("L");
          break;
        case STQ:
          Dib2Root.ccmSto.variableForceOrIndex("Q", args[0]);
          out0 = args[0];
          break;
        default:
          return null;
      }
    } catch (Exception e) {
      out0 = null;
    }
    return (null == out0) ? QWord.NaN : out0;
  }

  /*
  QIfs.QWordIf[] MOVE_ME_TO_SLOW__Exec(QOpMain funct, QSeq[] args) {
    QIfs.QWordIf[] result = new QIfs.QWordIf[1];
    if ((null == args) || (0 >= args.length)) {
      return null;
    }
    try {
      String file;
      byte[] dat;
      boolean ok;
      switch (funct) {
        case FDECD:
        case FENCD:
          ok = false;
          //        if ((null != args) && !QMap.isEmpty( QVal.asQVal( args[ 0 ] ) )) {
          file = args[0].toStringFull();
          if ((null != file) && (0 < file.length())) {
            try {
              if (0 > file.indexOf('/')) {
                file = new File(Dib2Root.platform.getFilesDir("external"), file).getAbsolutePath();
              }
              if (QOpMain.FDECD == funct) {
                dat = TcvCodec.instance.readPacked(file + ".dib", null, null, null);
                MiscFunc.writeFile(file, dat, 0, dat.length, null);
              } else {
                dat = MiscFunc.readFile(file, 0);
                ok = (0 < TcvCodec.instance.writePacked(dat, 0, dat.length, file + ".dib"));
              }
            } catch (Exception e) {
            }
          }
          result[0] = ok ? QWord.TRUE : QWord.FALSE;
          break;
        case EXPALL:
        case EXPORT:
          ok = false;
          file = args[0].toStringFull();
          if ((null != file) && (0 < file.length())) {
            // Do not expose key values etc. unless really requested
            dat = null; // Dib2Root.ccmSto.toCsv(null, 0, ~0, (funct == QcCalc.EXPALL) ? (~0) : 4);
            try {
              if (0 > file.indexOf('/')) {
                file = new File(Dib2Root.platform.getFilesDir("external"), file).getAbsolutePath();
              }
              //              MiscFunc.writeFile(file, dat, 0, dat.length, null);
              ok = true;
            } catch (Exception e) {
            }
          }
          result[0] = ok ? QWord.TRUE : QWord.FALSE;
          break;
        case IMPORT:
          ok = false;
          file = args[0].toStringFull();
          if ((null != file) && (0 < file.length())) {
            try {
              if (0 > file.indexOf('/')) {
                file = new File(Dib2Root.platform.getFilesDir("external"), file).getAbsolutePath();
              }
              dat = MiscFunc.readFile(file, 0);
              if (Dib2Constants.MAGIC_BYTES.length < dat.length) {
                if ((byte) Dib2Constants.RFC4880_EXP2 == dat[0]) {
                  ok = false; // 0 < Dib2Root.ccmSto.importFile(file, false);
                } else if (Arrays.equals(
                    Dib2Constants.MAGIC_BYTES,
                    Arrays.copyOf(dat, Dib2Constants.MAGIC_BYTES.length))) {
                  // ok = 0 < Dib2Root.ccmSto.importCsv(dat, false, 0);
                } else {
                  ///// Expecting simple entries or a header line.
                  boolean found = false;
                  for (int i0 = 0; (i0 < 100) && (i0 < dat.length); ++i0) {
                    if ('\t' == dat[i0]) {
                      found = true;
                      break;
                    }
                  }
                  for (int i0 = 0; (i0 < 1000) && (i0 < dat.length); ++i0) {
                    if ('\n' == dat[i0]) {
                      found = found && true;
                      break;
                    }
                  }
                  if (found) {
                    // ok = 0 < Dib2Root.ccmSto.importCsv(dat, false, 0);
                  }
                }
              }
            } catch (Exception e) {
            }
          }
          result[0] = ok ? QWord.TRUE : QWord.FALSE;
          break;
        case SAVTO:
          ok = false;
          file = args[0].toStringFull();
          if ((null != file) && (0 < file.length())) {
            if (0 > file.indexOf('/')) {
              file = new File(Dib2Root.platform.getFilesDir("external"), file).getAbsolutePath();
            }
          //ok = 0 < IoRunner.write(file, true, false, true);
          }
          result[0] = ok ? QWord.TRUE : QWord.FALSE;
          break;
        default:
          return null;
      }
    } catch (Exception e) {
      result = null;
    }
    //  cmd = null;
    return ((null == result) || ((1 <= result.length) && (null == result[0]))) ? null : result;
  }
  */

  static void importExternalTsv(String[] lines, long updateFlags, QWord ctrbFallback) {
    CcmTag[] map = null;
    QSTuple[] out = new QSTuple[lines.length];
    for (int i0 = 0; i0 < lines.length; ++i0) {
      if ((null == lines[i0]) || (0 > lines[i0].indexOf('\t'))) {
        continue;
      } else if (null == map) {
        if (((lines[i0].charAt(0) == Dib2Constants.MAGIC_BYTES[0])
                && (lines[i0].charAt(1) == Dib2Constants.MAGIC_BYTES[1]))
            || (lines[i0].contains("\tTIME\t")
                && lines[i0].contains("\tDAT")
                && lines[i0].contains("\tCAT"))) {
          map = CcmSto.createTagMap(lines[i0].split("\t"));
          continue;
        } else {
          int cFields = lines[i0].split("\t").length;
          map = CcmSto.getTagMapStd((7 <= cFields) ? 7 : 3);
        }
      }
      final QSTuple mpg = CcmSto.createTuple4Tsv(lines[i0], map);
      out[i0] = mpg;
    }
    out = CcmSto.ccmCleanup(out, updateFlags, QWord.V_1);
    CcmSto.importData(false, updateFlags, 0, out);
  }

  static QEnumIf execOpWk(QToken tok) {
    switch ((QOpWk) tok.op) {
      case zzDATA:
        {
          if (!(tok.wip instanceof QSTuple[])) {
            ExceptionAdapter.throwAdapted(
                new IllegalArgumentException(), null, "zzDATA: " + tok.stamp);
          }
          QSTuple[] dat = (QSTuple[]) tok.wip;
          dat = CcmSto.ccmCleanup(dat, -1L, QWord.V_0);
          tok.wip = CcmSto.importData(true, -1L, 4L, dat);
        //Dib2Root.ccmSto.variableForceOrIndex("L", QSeq.createQSeq("" + dat.length + " records"));
          return QOpFeed.zzFILL;
        }
      case zzIMPORT:
        {
          if (!(tok.wip instanceof String[])) {
            ExceptionAdapter.throwAdapted(
                new IllegalArgumentException(), null, "zzIMPORT: " + tok.stamp);
          }
          importExternalTsv(
              (String[]) tok.wip, (Cats.CHAT.flag | Cats.MSG.flag | Cats.DONE.flag), QWord.V_1);
          return null;
        }
      case zzQSTO:
        {
          if ((null == tok.parS0) || (0 >= tok.parS0.length()) || (0 == tok.parN0) || (null == tok.argX)
              || (0 == tok.argX.getShash())) {
            return null;
          }
          final String dat = (tok.argX instanceof QSeq) ? ((QSeq) tok.argX).toStringFull() : tok.argX.toString();
          final QWord label = QWord.createQWord(tok.parS0, true);
          QSTuple entry = CcmSto.createEntry(label, tok.parN0, QSeq.createQSeq(StringFunc.string4Mnemonics1345(dat, 0L)));
          Dib2Root.ccmSto.zMappingsPid.add4Handle(entry);
          Dib2Root.ccmSto.zMappingsLabel.add4Multi(label.shash, entry);
          //QWord.createQWordInt(entry.getShash());
          return null;
        }
      case zzQUP: 
        {
          if (tok.argX instanceof QSeq) {
            QIfs.QItemIf item = Dib2Root.ccmSto.zMappingsPid.search(tok.parN0);
            if (item instanceof QSTuple) {
              ((QSTuple) item).set(CcmTag.DAT, tok.argX);
            }
          }
          return null;
        }
      case zzARCHIVE: {
        QSTuple[] dat = CcmSto.archiveData();
        if (null != dat) {
          tok.wip = IoRunner.exportTsv(dat, CcmSto.CcmTag.tsvFieldNames());
          return QOpIo.zzSAVEARCHIVE;
        }
        return null;
      }
      case zzSAV1:
      case zzSAV1_zzEXIT:
        {
          final QSeqIf[] dat = (QSeqIf[]) tok.wip;
          tok.wip =
              IoRunner.exportTsv(
                  CcmSto.exportData(dat, dat.length, -1L, -1L), CcmSto.CcmTag.tsvFieldNames());
          return (QOpWk.zzSAV1_zzEXIT == tok.op) ? QOpIo.zzSAV2_zzEXIT : QOpIo.zzSAV2;
        }
      default:
        ;
    }
    return null;
  }
}
