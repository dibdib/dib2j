// Copyright (C) 2018, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.config;

// =====

public final class Dib2Lang {

  // =====

  public static final String[] kLanguages = {"EN", "DE"};

  // =====

  public static enum AppState {

    // =====

    CREATE("START", "START"),
    INIT("INIT", "INIT"),
    DISCLAIMER("BEWARE!", "BEACHTEN!"),
    // For loading encoded data ...
    LOGIN("LOAD", "LADEN"),
    ACTIVE("OK", "OK"),
    // BACKGROUND("BUSY", "VERARBEITUNG"),
    EXIT_REQUEST("EXIT", "ENDE"),
    EXIT_TRIGGERED("EXIT", "ENDE"),
    EXIT_DONE("EXIT", "ENDE"),
    ;

    public final String[] transls = new String[kLanguages.length];

    private AppState(String en, String de) {
      transls[0] = en;
      transls[1] = de;
    }
  }

  // =====

  public static final String[] kUiAgree = {
    "EN:\tThis program comes with",
    "\tABSOLUTELY NO WARRANTY.",
    "\tDo not use this program if you",
    "\tdo not agree to that in its",
    "\tfullest possible sense.",
    "DE:\tBenutzen Sie das Programm",
    "\tnicht ohne den englischen Text",
    "\tzu verstehen und zuzustimmen!",
    "",
    "\tTap green '>' to start.",
    "\tTap \u2297 (top left) to reset. ",
    "\tTap 'VW' to see the license.",
    "\tTap 'GO' to skip introduction.",
    "",
  };

  public static final String[] kLicensePre = {
    "", // ,
    "(Lizenz: Nur auf Englisch. Im Zweifelsfall Programm nicht benutzen!)",
    "(Tap ESCAPE (red X, top left) to return.) ",
    "(Beenden mit ESCAPE (rotes X, oben links) ...)",
    "(2)",
    "(2)",
  };

  public static final String[] kFeedWait4Proc = {
    "Processing ...", // ,
    "Datenbearbeitung ...",
  };

  public static final String[] kFeedLoadSave = {
    "\tPreparing data ...", // ,
    "\tDatenzugriff ...",
  };

  public static final String[] kUiIntro100 = {
    "\t    (Area of blue scroll bars active)",
    "EN:\tYou can switch the language",
    "\tby tapping LA-NG (above, left)",
    "\tOtherwise continue with '>'.",
    "DE:\tUm die Sprache umzuschalten,",
    "\tbitte LA-NG (oben links) druecken.",
    "\tDann mit '>' fortfahren.",
    "\t(Sprachunterstuetzung z.Zt. beschraenkt)",
    "",
    "\tES / FR / ... ",
    "\t(Somebody willing to help translate?)",
    "",
  };

  public static final String[] kUiIntro110 = {
    //
    "\tNOTE:",
    "\tANMERKUNG:",
    //
    "\tESC (red button top left)",
    "\tESC (roter Schalter oben links)",
    //
    "\tskips introduction.",
    "\tueberspringt Einfuehrung.",
    //
    "\tZOOM buttons (next to it)",
    "\tDie ZOOM Schalter (daneben)",
    //
    "\tadjust the text size.",
    "\taendern die Textgroesse.",
    //
    "",
    "",
    //
    "\tContinue with '>'.",
    "\tMit '>' fortfahren.",
    //
    "",
    "",
  };

  public static final String[] kUiStepAcLoad_x = {
    "",
    "",
    Dib2Constants.NO_WARRANTY[0],
    "",
    "",
    "",
    "EN:  Loading data ...",
    "DE:  Daten werden geladen ...",
    "",
    "",
  };

  public static final String[] kUiStepAc = {
    Dib2Constants.NO_WARRANTY[0],
    "",
    "EN: Please enter your access code (PIN) and",
    "    tap the green '>'.",
      //"    (Note: ESCAPE (top left) renames the file)",
    "DE: Bitte Zugriffscode (PIN) eingeben und dann",
    "    gruenes '>' druecken.",
    "",
    "(Note: this freely choosable access code/ PIN",
    "gives you faster access to the data later on.)",
    "",
  };

  public static final String[] kUiStepAcOpt = {
    Dib2Constants.NO_WARRANTY[0],
    "",
    "EN: Enter access code and",
    "    then tap the green '>'",
    "    -- or tap ESCAPE button (top left)",
    "    to suppress the extra passphrase.",
    "DE: Bitte Zugriffscode eingeben und dann",
    "    gruenes '>' druecken -- oder ESCAPE (oben links).",
    "    um den Zugriffscode zu unterdruecken.",
  };

  public static final String[] kUiStepAcWait = {
    "",
    "",
    Dib2Constants.NO_WARRANTY[0],
    "",
    "EN: Please wait, then enter your access code",
    "    and tap the green '>' (bottom).",
    "DE: Bitte warten, dann Zugangscode eingeben",
    "    und gruenes '>' (unten) druecken.",
    "",
  };

  public static final String[] kUiStepPw = {
    Dib2Constants.NO_WARRANTY[0],
    "",
    "EN: Please enter your password and",
    "    tap the green '>' (bottom).",
    "    (Note: ESCAPE deletes the access code)",
    "DE: Bitte Passwort eingeben",
    "    und gruenes '>' (unten) druecken.",
    "    (ESCAPE loescht den Zugangscode)",
    "",
    "(Note: this is your e-mail password",
    "for accessing your online e-mail account)",
    "",
  };
  
  public static final String[] kUiNeedEMailAddress = {
    "",
    "",
    "EN: Please enter your e-mail address in",
    "    order to use it for messaging.",
    "    Afterwards tap the green '>'.",
    "DE: Bitte E-Mail-Adresse eingeben, um",
    "    sie für Chat-Nachrichten zu verwenden.",
    "    Danach gruenes '>' druecken.",
    "",
    };

  public static final String[] kUiNeedEMailHost = {
    "",
    "",
    "EN: Please confirm or change the host",
    "    data of your provider: hostname, user",
    "    name, ports for IMAP and SMTP.",
    "DE: Bitte die Daten des E-Mail-Providers (Host)",
    "    bestaetigen oder korrigieren: Hostname,",
    "    Benutzername, IMAP und SMTP Port.",
    "",
  };

  public static final String[] kUiIntroChat0 = {
    "",
    "\tINTRODUCTION",
    "",
    "Experimental version with overlay keyboard:",
    "Tap on the (red) 'A' on the",
    "top bar repeatedly to see the effect.",
    "",
    "Notice the 4 navigation keys in the corners.",
    "You can also tap on the sides: scrollbar.",
    "The red 'X' (top left) is the ESC (reset)",
    "button, next to it are the ZOOM buttons.",
    "Try it out!",
    "The status bar (bottom) handles page",
    "navigation: '>>' = forward, '<<' = backward,",
    "('>' and '<' for supplementary pages)",
    "Tap on '>>' to go to the next page.",
    "",
    "(Cmp. gitlab.com/gxworks/dib2qm)",
  };
  
  public static final String[] kUiIntroChat_Supp = {
    "",
    "",
    "",
    "(This is a",
    "supplementary page)",
    "",
    "Tap on '<'",
    "on the bottom bar (status bar)",
    "to return to the main page",
  };
  
  public static final String[] kUiIntroChat1 = {
    "",
    "\t(page 2)",
    "",
    "On the right of the top bar you will",
    "find the menu button (not supported yet,",
    "currently only for dark mode) and the",
    "clipboard buttons. Below are the",
    "tool bar and the entry bar. Tools:",
    "VW.. (= VieW ..) for flipping through the viewing modes,",
    "CLR (= CLeaR) for removing temporary data (from stack),",
    "SEND, RECeiVe etc. after ending this introduction.",
    "",
    "Note: some of those tools (operators)",
    "require an entry or extra parameters",
    "before being pressed. For example,",
    "before pressing SEND, the message",
    "to be sent has to be entered.",
    "",
    "Tap on '>>' to go to the next page.",
  };

  public static final String[] kUiIntroChat2 = { 
    "",
    "\t(page 3)",
    "",
    "After ending this introduction, you",
    "will be asked to enter your",
    "networking data, i.e. the necessary",
    "data for accessing your e-mail account.",
    "We recommend to use an extra/ dedicated",
    "account for this messenger.",
    "",
    "First: your e-mail address,",
    "Second: a confirmation of the technical data,",
    "Third: a freely choosable PIN/ access code,",
    "Fourth: your e-mail password.",
    "",
    "Every step will have to be completed by the",
    "green '>' (on the entry bar or status bar).",
    "",
    "Tap on '>>' to go to the next page.",
  };
    
  public static final String[] kUiIntroChat3 = { 
    "",
    "\t(page 4)",
    "",
    "After that setup, this would be",
    "a typical flow of commands:",
    "- Use the (greenish) keyboard to",
    "    type the e-mail address of your friend.",
    "- Press CON+ (= CONtact+) to add this",
    "    address to your data, creating a chat.",
    "- Press on that new chat and then on",
    "    INVIte to send an invitation",
    "    (note the negative page count in the status bar).",
    "- Your invitation will appear on your",
    "    friend's app as a new chat if he presses RECV.",
    "- Your friend also has to press on the chat and",
    "    INVI on his side to send his contact data.",
    "- Press RECeiVE to get his contact data",
    "    (incl. the 'fingerprint' for security).",
    "- A '?' on the second line requires ACKnowledge.",
    "- Type a short hello and press SEND on both sides.",
    "- Use RECV to get each others's hello.",
    "",
    "Press the 'push' button",
    "(the green '>' on the entry bar",
    "or the status bar) to get started.",
    "(Or '>>' for more information)",
  };
    
  public static final String[] kUiIntroChat4 = { 
      "",
      "\t(page 5)",
      "",
      "Other tools:",
      "",
      "- ACKnowledge is for confirming messages or invitations.",
      "- CHT+ is for additional chats with a specific topic",
      "    (enter the topic, press CHT+, then press on the new chat,",
      "    then '<<' and CON+ to add contacts to the chat,",
      "    finally press on the chat).",
      "",
      "In order to create a backup file,",
      "type 'xx.bak', press '>' (PUSH),",
      "then type 'savto' and press GO.",
      "",
      "Note the so-called fingerprint that follows the",
      "e-mail addresses. This is the security code, which",
      "you can use to ensure proper encryption.",
      "A leading '?' requires ACK!",
      "",
      "Press the 'push' button",
      "(the green '>' on the entry bar",
      "or the status bar) to get started.",
      "",
      "(Cmp. gitlab.com/gxworks/dib2qm)",
    };

  public static final String[][] kUiIntroChat = new String[][] {
    kUiIntroChat0,
    kUiIntroChat1,
    kUiIntroChat2,
    kUiIntroChat3,
    kUiIntroChat4,
  };

  public static final String[] kWelcome_CALC = {
    "Dib2Calc (a.k.a. Dibdib Calculator).", // ,
    "Dib2Calc (genannt Dibdib Calculator).",
  };

  public static final String[] kUiStep600x = {
    //
    "",
    "",
    //
    "EN: Tap '>' (above on the right",
    "DE: Fortsetzen mit '>'",
    //
    "       or bottom) to continue.",
    "       (oberhalb rechts oder unten)",
    //
    "",
    "",
    //
    "EN: Switch language with LA-NG.",
    "EN: Switch language with LA-NG.",
    //
    //
    "DE: Sprache mit LA-NG umschalten.",
    "DE: Sprache mit LA-NG umschalten.",
    //
    "",
    "",
  };

  public static final String[] kUiStep610 = {
    "",
    "",
    //
    "EN: The LA-NG (top left) button",
    "DE: LA-NG (oben links) schaltet die",
    //
    "      switches the language.",
    "      Sprache um.",
    //
    // A long click switches the keyboard.",
    // Tastatur umschalten mit langem Klick.",
    //
    "      (Above that: ESC and ZOOM buttons).",
    "      (Oberhalb: ESC und ZOOM).",
    //
    "",
    "",
    //
    "      Tap '>' (bottom) to continue.",
    "      Mit '>' (unten) fortsetzen.",
    //
    "",
    "",
    //
    "DE: Mit LA-NG (oben links) Sprache",
    "EN: Use LA-NG (top left) to switch",
    //
    "      umschalten.",
    "      the language.",
  };

  public static final String[] kUiIntroCalc200 = {
    "X\t2",
    "X\t2",
    //
    "Y\t3",
    "Y\t3",
    //
    "",
    "",
    //
    "\t(INTRODUCTION:)",
    "\t(EINFUEHRUNG:)",
    //
    "\tX=2, Y=3 as shown above.",
    "\tX=2, Y=3 wie oben angezeigt,",
    //
    "\tTap green '+' (below)",
    "\tGruenes '+' (unten) druecken,",
    //
    "\tto add those values.",
    "\tum die Werte zu addieren.",
    //
    // "\t(3 + 2) => [3 2 +] = 5",
    //
    "",
    "",
  };

  public static final String[] kUiIntroCalc210 = {
    "X\t5",
    "X\t5",
    //
    //
    "",
    "",
    //
    "\tTap '9' and then '>'",
    "\t'9' und dann '>' oder",
    //
    "\tor ENTER (bottom right,",
    "\tENTER (unten, oberhalb '>')",
    //
    "\tabove '>') to push a",
    "\tdruecken, um einen Wert",
    //
    "\tnew value.",
    "\teinzufuegen.",
    //
    "",
    "",
  };

  public static final String[] kUiIntroCalc220 = {
    "X\t9",
    "X\t9",
    //
    "Y\t5",
    "Y\t5",
    //
    "",
    "",
    //
    "\tTap ENTER (bottom right,",
    "\tENTER druecken (unten rechts,",
    //
    "\tabove '>') in order to duplicate",
    "\toberhalb '>'), um den Wert X",
    //
    "\tthe top value.",
    "\tzu kopieren.",
    //
    "",
    "",
  };

  public static final String[] kUiIntroCalc230 = {
    "X\t9",
    "X\t9",
    //
    "Y\t9",
    "Y\t9",
    //
    "Z\t5",
    "Z\t5",
    //
    "",
    "",
    //
    "\tTap '*' to multiply the",
    "\t'*' druecken, um die ersten zwei",
    //
    "\tfirst 2 values.",
    "\tWerte zu multiplizieren.",
    //
    "",
    "",
  };

  public static final String[] kUiIntroCalc240 = {
    "X\t81",
    "X\t81",
    //
    "Y\t5",
    "Y\t5",
    //
    "",
    "",
    //
    "\tTap '*' again.",
    "\t'*' nochmal druecken.",
    //
    "\tNOTE:",
    "\tANMERKUNG:",
    //
    "\tUse 'GO' for operator names, e.g.:",
    "\tFunktionsnamen anwenden mit 'GO':",
    //
    "\tInstead of '+' you could type 'ADD'",
    "\tAnstatt '+' kann auch 'ADD' eingegeben",
    //
    "\tand then tap GO (top bar).",
    "\twerden, gefolgt von dem GO Schalter",
    //
    "",
    "\t(in Leiste oben)",
    //
    //
    "",
    "",
  };

  public static final String[] kUiIntroCalc250 = {
    "X\t405",
    "X\t405",
    //
    "",
    "",
    //
    "\tTap C-LR to delete the data.",
    "\tMit C-LR Daten loeschen.",
    //
    "",
    "",
    //
    "\t(Then try some entries and",
    "\t(Probieren Sie dann ein paar",
    //
    "\tcalculations as just done.)",
    "\tEingaben und Rechnungen",
    //
    "",
    "\twie gerade getan.)",
    //
    "",
    "",
  };

  public static String[] pickTransl(String[] xList) {
    String[] out = new String[xList.length / kLanguages.length];
    int count = 0;
    for (int i0 = Dib2Root.ui.iLang; i0 < xList.length; i0 += kLanguages.length) {
      out[count++] = xList[i0];
    }
    return out;
  }

  // =====
}
