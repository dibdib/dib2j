// Copyright (C) 2016, 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package net.sf.dibdib.thread_any;

import java.util.*;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;

/** Basic methods plus dummy implementation of interface for debugging. */
public class CodecFunc implements TsvCodecIf {
  // =====

  public static final CodecFunc instance = new CodecFunc();

  @Override
  public byte[] create(char platform, Object... parameters) {
    return null;
  }

  @Override
  public byte[] compress(byte[] xyData, int xOffset4Reuse, int to) {
    return MiscFunc.compress('z', xyData, xOffset4Reuse, to);
  }

  @Override
  public byte[] decompress(byte[] xData, int len) {
    return MiscFunc.decompress(xData, 0, len);
  }

  @Override
  public byte[] getKey(byte[] phrase, byte[] accessCode, byte[] salt, int iterations) {
    return phrase;
  }

  @Override
  public String getKeyInfo(HashSet<String> entries) {
    return "(keyInfo)";
  }

  @Override
  public byte[] encode(
      byte[] compressedData,
      int from,
      int to,
      byte[] key,
      byte[] iv16,
      int keyInfo,
      byte[] keyData,
      byte[] signatureKey)
      throws Exception {
    return Arrays.copyOfRange(compressedData, from, to);
  }

  @Override
  /** For debugging: pass is not used */
  public byte[] decode(byte[] data, int offset, int len, byte[] keyOrPass, byte[] signatureKey)
      throws Exception {
    int offs = offset;
    if (data[offs] == (byte) Dib2Constants.RFC4880_EXP2) {
      offs += MiscFunc.getPacketHeaderLen(data, offs);
    } else {
      if ((data[offs] != Dib2Constants.MAGIC_BYTES[0])
          || (data[offs + Dib2Constants.MAGIC_BYTES.length + 1] != getMethodTag())) {
        return null;
      }
    }
    return Arrays.copyOfRange(data, offs + 16, offset + len);
  }

  @Override
  public byte getMethodTag() {
    return '0'; // dummy - plain
  }

  @Override
  public byte[] getInitialValue(int len) {
    return Arrays.copyOf("0123456789".getBytes(StringFunc.CHAR8), len);
  }

  @Override
  public byte[] encodePhrase(byte[] phrase, byte[] accessCode) {
    return phrase;
  }

  @Override
  public byte[] decodePhrase(byte[] encoded, byte[] accessCode) {
    return encoded;
  }

  // =====
}
