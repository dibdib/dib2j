// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.dib2j;

import static net.sf.dibdib.config.Dib2Root.CmdLineArgs.*;

import com.gitlab.dibdib.awt_ui.FrameAwt;
import com.gitlab.dibdib.common.TcvCodecAes;
import com.gitlab.dibdib.picked.net.MessengerQm;
import com.gitlab.dibdib.tty_ui.AppTty;
import java.io.InputStream;
import java.net.*;
import javax.activation.URLDataSource;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;
import net.sf.dibdib.thread_net.*;

// =====

public final class Main extends AppTty {

  // =====

  private static final Main INSTANCE_TERMINAL = new Main();

  public static void main(String[] args) throws Exception {


    //TODO
    name = "Dib2J";
    final String nameShort = "j";
    
    
    FrameAwt.platformSecondary = INSTANCE_TERMINAL;
    QOpNet.messenger = new MessengerQm();
    Dib2Root.app.bAllowDummyPass = true;

    Dib2Root.scanArgs(args);
    if (dumpHelp()) {
      return;
    }
    final TsvCodecIf[] codecs = new TsvCodecIf[] {TcvCodecAes.instance};
    if ((null != TTY.value) || (null != CARRIAGERETURN.value)) {
      final int width = (null == WIDTH.value) ? 76 : Integer.parseInt(WIDTH.value);
      // 38 double lines = 76:
      final int height = (null == HEIGHT.value) ? 76 : Integer.parseInt(HEIGHT.value);
      INSTANCE_TERMINAL.create(
          name, width, height, '0', "tty", INSTANCE_TERMINAL, INSTANCE_TERMINAL, codecs);
      INSTANCE_TERMINAL.configure();
      if (INSTANCE_TERMINAL.start()) {
        INSTANCE_TERMINAL.run();
      }
      return;
    }

    final int zoomLvl = (null == ZOOM.value) ? 0 : Integer.parseInt(ZOOM.value);
    final int dpi = (null == DPI.value) ? 128 : Integer.parseInt(DPI.value);
    final int width = (null == WIDTH.value) ? 360 << (zoomLvl / 2) : Integer.parseInt(WIDTH.value);
    final int height = // 480
        (null == HEIGHT.value) ? 600 << (zoomLvl / 2) : Integer.parseInt(HEIGHT.value);
    final FrameAwt frameAwt =
        new FrameAwt(name, '0', nameShort, Dib2Root.platform, (Runnable) Dib2Root.platform, codecs);
    frameAwt.configureNStart(width, height, dpi, zoomLvl);
  }

  @Override
  public InputStream openInputStreamGetType(Object xUri, String[] yType) {
    try {
      URL url = ((URI) xUri).toURL();
      URLDataSource ds = new URLDataSource(url);
      InputStream is = ds.getInputStream();
      yType[0] = ds.getContentType();
      return is;
    } catch (Exception e) {
    }
    return null;
  }

  public Object parseUri(String path) {
    return URI.create(path);
  }

  // =====
}
