// Copyright (C) 2020, 2023  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.awt_ui;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.InputStream;
import java.net.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_ui.*;

// =====

public class FrameAwt extends Frame implements ClipboardOwner {

  // =====

  private static final long serialVersionUID = 0xD1bD1b441772L;

  static FrameAwt instance = null;
  private final CanvasAwt mCanvas;
  private int mDpi = 96;

  public static PlatformFunc platformSecondary = null;

  public FrameAwt(
      String title,
      char xPlatformMarker,
      String xAppShort,
      QIfs.PlatformIf xPlatform,
      Runnable xWorker,
      TsvCodecIf[] xmCsvCodecs) {
    super();
    instance = this;
    setTitle(title);
    Dib2Root.platform = (null != xPlatform) ? xPlatform : new Platform();
    final UiFrame frame = Dib2Root.create(xPlatformMarker, xAppShort, null, xmCsvCodecs);
    Dib2Root.app.bPermitted = true;
    mCanvas = new CanvasAwt(frame);
    UiValTag.UI_BAR_BACKGROUND_COLOR.setInitial(ColorNmz.ColorDistinct.SHADE__WHITESMOKE.rgb0);
  }

  public void configureNStart(
      int width, int height, int dpi, int zoomLvl, String... fontNamesSysMonoSerif) {
    mDpi = dpi;
    Dib2Root.init(Dib2Root.app.bAllowDummyPass);
    Dib2Root.resume();
    if (null != Dib2Root.CmdLineArgs.X0.value) {
      Dib2Root.app.dbFileName = Dib2Root.CmdLineArgs.X0.value;
    }
    addWindowListener(new Dib2WindowListener());
    Toolkit.getDefaultToolkit().addAWTEventListener(new Dib2TabListener(), -1L);
    add(mCanvas);
    setSize(width + 10, height + 30);
    mCanvas.configure(fontNamesSysMonoSerif);
    setVisible(true);
    mCanvas.requestFocusInWindow();
    checkSizeNZoom();
    UiValTag.UI_ZOOMLVL_BOARD.setInitial(zoomLvl);
  }

  void checkSizeNZoom() {
    final Insets insets = getInsets();
    final Dimension size = getSize();
    final int wid = (int) size.getWidth() - insets.right - insets.left;
    final int hgh = (int) size.getHeight() - insets.bottom - insets.top;
    final int min = (wid <= hgh) ? wid : hgh;
    mCanvas.setSize(wid, hgh);
    mCanvas.setBounds(insets.left, insets.top, wid, hgh);
    UiFrame.checkSizeNZoom(wid, hgh, min * Dib2Constants.UI_PT10_P_INCH / mDpi);
  }

  void closeFrame(boolean actively) {
    boolean ok = Dib2Root.onCloseApp(3500);
    dispose();
    if (actively) {
      System.exit(ok ? 0 : 1);
    }
  }

  // =====
  class Dib2WindowListener extends WindowAdapter {
    // =====
    @Override
    public void windowClosing(WindowEvent e) {
      e.getWindow().dispose();
      closeFrame(false);
    }
    // =====
  }

  // =====
  class Dib2MouseMotionListener extends MouseMotionAdapter {
    // =====
    @Override
    public void mouseDragged(MouseEvent e) {
      mCanvas.repaint();
    }
    // =====
  }

  // =====
  class Dib2TabListener implements AWTEventListener {

    // ==> Left, Right, Up, Down / ^L,... CVX, cliQ, 2..9.. pre as factor

    long timeKeyPressedOrReleased = 0;

    @Override
    public void eventDispatched(AWTEvent event) {
      if (event instanceof KeyEvent) {
        final long time = DateFunc.currentTimeMillisLinearized();
        if ((time - 650) < timeKeyPressedOrReleased) {
          return;
        }
        timeKeyPressedOrReleased = time;
        final char k = ((KeyEvent) event).getKeyChar();
        if ('\t' == k) {
          UiPres.INSTANCE.handleKey(k, false);
          mCanvas.repaint();
        }
        timeKeyPressedOrReleased = DateFunc.currentTimeMillisLinearized();
      } else if (event instanceof FocusEvent) {
        mCanvas.requestFocusInWindow();
        timeKeyPressedOrReleased = DateFunc.currentTimeMillisLinearized();
      } else if (event instanceof WindowEvent) {
        instance.invalidate();
      }
    }

    // =====
  }

  // =====
  static class Dib2KeyListener extends KeyAdapter {
    // =====

    @Override
    public void keyTyped(KeyEvent e) {
      char k = e.getKeyChar();
      UiPres.INSTANCE.handleKey(k, false);
    }
    // =====
  }

  // =====
  static class Dib2MouseListener extends MouseAdapter {
    // =====

    @Override
    public void mousePressed(MouseEvent e) {
      final int xMouse = e.getX();
      final int yMouse = e.getY();
      UiPres.INSTANCE.handleMouse(xMouse, yMouse);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      ClickRepeater.stopMouseRep();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
      ClickRepeater.stopMouseRep();
    }
    // =====
  }

  /////

  public static final class Platform extends PlatformFunc implements Runnable {

    @Override
    public boolean pushClipboard(String label, String text) {
      final StringSelection sel = new StringSelection(text);
      instance.setClipContent(sel);
      return true;
    }

    @Override
    public String getClipboardText() {
      String out = "";
      final Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
      final Transferable dat = clip.getContents(null);
      if ((null != dat) && dat.isDataFlavorSupported(DataFlavor.stringFlavor)) {
        try {
          out = (String) dat.getTransferData(DataFlavor.stringFlavor);
        } catch (Exception e0) {
          return null;
        }
      }
      return out;
    }

    @Override
    public void log(String... aMsg) {
      if (null != Dib2Root.CmdLineArgs.DEBUG.value) {
        System.out.println(Arrays.toString(aMsg));
      }
    }

    @Override
    public void toast(String msg) {
      System.out.println(msg);
    }

    @Override
    public void invalidate() {
      instance.checkSizeNZoom();
      instance.invalidate();
    }

    @Override
    public void run() {
      MainThreads.TOPNET.trigger(null);
      // No need to wait in this case ...
    }

    @Override
    public InputStream openInputStreamGetType(Object xUri, String[] yType) {
      return (null == platformSecondary)
          ? null
          : platformSecondary.openInputStreamGetType(xUri, yType);
    }

    @Override
    public Object parseUri(String path) {
      try {
        return new URI(path);
      } catch (URISyntaxException e) {
      }
      return null;
    }
  }

  /////

  @Override
  public void update(Graphics g) {
    // No super.update(): avoid flicker.
    paint(g);
  }

  @Override
  public void invalidate() {
    super.invalidate();
    mCanvas.repaint();
  }

  public void setClipContent(StringSelection sel) {
    final Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    clip.setContents(sel, this);
  }

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {}

  // =====
}
