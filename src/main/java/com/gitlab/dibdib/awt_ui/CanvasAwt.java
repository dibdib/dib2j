// Copyright (C) 2020, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.awt_ui;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.VolatileImage;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.QIfs.GraphicsIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiFrame;

// =====

class CanvasAwt extends Component implements GraphicsIf {

  // =====

  private static final long serialVersionUID = -5971121882913247207L;

  final UiFrame rFrame;

  /** 2*72 dpi as base value: shifting one less than the reference 72*1024 vpx/in. */
  static int zShift2Px_0 = Dib2Constants.UI_PT10_SHIFT - 1;

  private static final int[] zColorVals = new int[32];
  private static final Color[] zColors = new Color[32];
  private static int ziColorNext = 0;

  static final String[] zFontFav = {
    "NOTO SANS MONO", "FREEMONO", "ARIAL", "DEJAVU SANS", "TIMES NEW ROMAN", "FREESERIF"
  };
  static final String[] zFontsFallback = {"MONO", "SANS", "SERIF"};
  final String[] zFontNamesMonoSysSerif = new String[3];
  Font[] zFonts128MonoSysSerif = new Font[3];

  private static final String kAlphaOmega = "abcdefghijklmnopqrstuvwxyz";
  private static final String kDigits = "0123456789";

  private Graphics2D zGraphics;
  private VolatileImage[] zImages = new VolatileImage[8];
  private int[] zImageBorderColor = new int[8];
  private Graphics2D zGraphicsTmp;
  private Color zColorTool;
  private Color zColorText;
  private int zSmallCharsPerDot = 0;

  public CanvasAwt(UiFrame xrFrame) {
    super();
    rFrame = xrFrame;
    addKeyListener(new FrameAwt.Dib2KeyListener());
    addMouseListener(new FrameAwt.Dib2MouseListener());
    final Color bg = new Color(248, 248, 248);
    setBackground(bg);
  }

  public void configure(String... fontNamesSysMonoSerif) {
    final int nFonts8Fam = zFontFav.length / 3;
    if (null != fontNamesSysMonoSerif) {
      if ((0 < fontNamesSysMonoSerif.length) && (2 < fontNamesSysMonoSerif[0].length())) {
        zFontFav[2 * nFonts8Fam - 1] = fontNamesSysMonoSerif[0];
      }
      if ((1 < fontNamesSysMonoSerif.length) && (2 < fontNamesSysMonoSerif[1].length())) {
        zFontFav[1 * nFonts8Fam - 1] = fontNamesSysMonoSerif[1];
      }
      if ((2 < fontNamesSysMonoSerif.length) && (2 < fontNamesSysMonoSerif[2].length())) {
        zFontFav[3 * nFonts8Fam - 1] = fontNamesSysMonoSerif[2];
      }
    }
  }

  private void initFonts(Graphics2D g) {
    final String[] potential = new String[3];
    final Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
    final int[] aeWid = new int[] {-9999, -9999, -9999};
    final String[] defaults = {Font.MONOSPACED, Font.SANS_SERIF, Font.SERIF};
    final int widMono = UiFunc.boundWidthMono(kAlphaOmega, 1 << Dib2Constants.UI_FONT_NMZ_SHIFT);
    final int widNmz = UiFunc.boundWidthNmz(kAlphaOmega, 1 << Dib2Constants.UI_FONT_NMZ_SHIFT);
    FontRenderContext frc = g.getFontRenderContext();
    for (Font font : allFonts) {
      if (!font.canDisplay('a') || !font.canDisplay('0')) {
        continue;
      }
      String fam = font.getFamily().toUpperCase(Locale.ROOT);
      for (int ix = 0; ix < zFontFav.length; ++ix) {
        if (zFontFav[ix].equals(fam)) {
          final int i0 = ix / (zFontFav.length / 3);
          if (hasSpecialChars(font, (1 == i0))) {
            this.zFontNamesMonoSysSerif[i0] = font.getFamily();
          } else if (null == potential[i0]) {
            potential[i0] = font.getFamily();
          }
        }
      }
      for (int i0 = 0; i0 < zFontsFallback.length; ++i0) {
        if (fam.contains(zFontsFallback[i0])) {
          if ((fam.startsWith("BITSTREAM")
                  || fam.startsWith("COURIER NEW")
                  || fam.startsWith("DEJAVU")
                  || fam.startsWith("LIBERATION"))
              && hasSpecialChars(font, 1 == i0)) {
            if ((null == this.zFontNamesMonoSysSerif[i0])
                || (this.zFontNamesMonoSysSerif[i0].length() > fam.length())) {
              this.zFontNamesMonoSysSerif[i0] = font.getFamily();
            }
          }
          if ((null == potential[i0]) || hasSpecialChars(font, 1 == i0)) {
            Font fx = font.deriveFont((float) (1 << Dib2Constants.UI_FONT_NMZ_SHIFT));
            TextLayout layout = new TextLayout("nxn", fx, frc);
            Rectangle2D bb = layout.getBounds();
            double wx = (int) ((bb.getWidth()));
            double hx = (int) ((bb.getHeight()));
            if ((wx < (Dib2Constants.UI_FONT_NMZ_N_ADV * 3 / 2))
                || (hx <= (Dib2Constants.UI_FONT_NMZ_X_HEIGHT / 2))
                || (hx >= (Dib2Constants.UI_FONT_NMZ_HEIGHT * 3 / 4))) {
              break;
            }
            layout = new TextLayout(kAlphaOmega, fx, frc);
            bb = layout.getBounds();
            wx = (int) ((bb.getWidth()));
            int ewid =
                (int) (wx * Dib2Constants.UI_FONT_NMZ_X_HEIGHT / hx)
                    - ((i0 == 0) ? widMono : widNmz);
            if ((null == potential[i0])
                || ((fam.length() < potential[i0].length()))
                    && potential[i0].toUpperCase(Locale.ROOT).startsWith(fam)) {
              potential[i0] = font.getFamily();
              aeWid[i0] = ewid;
            } else {
              if ((0 >= ewid)
                  ? ((aeWid[i0] < ewid) || ((aeWid[i0] * 2) > -ewid))
                  : (ewid < aeWid[i0])) {
                potential[i0] = font.getFamily();
                aeWid[i0] = ewid;
              }
            }
          }
          break;
        }
      }
    }
    for (int i0 = 0; i0 < this.zFontNamesMonoSysSerif.length; ++i0) {
      if (null == this.zFontNamesMonoSysSerif[i0]) {
        this.zFontNamesMonoSysSerif[i0] = (null != potential[i0]) ? potential[i0] : defaults[i0];
      }
      this.zFonts128MonoSysSerif[i0] = new Font(this.zFontNamesMonoSysSerif[i0], Font.PLAIN, 128);
    }
    g.setRenderingHint(
        RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON); // RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
  }

  @Override
  public void drawImage(int index, int left, int top) {
    final int wid = zImages[index].getWidth();
    final int hgh = zImages[index].getHeight();
    zGraphicsTmp.drawImage(zImages[index], left, top, this);
    if (0 != zImageBorderColor[index]) {
      zGraphicsTmp.setColor(getColor(zImageBorderColor[index]));
      zGraphicsTmp.drawRect(left - 1, top - 1, wid + 1, hgh + 1);
    }

    zGraphics = zGraphicsTmp;
  }

  @Override
  public void drawLine(int startX, int startY, int stopX, int stopY) {
    zGraphics.drawLine(startX, startY, stopX, stopY);
  }

  @Override
  public void drawText(String text, int x, int y) {
    zGraphics.setColor(zColorText);
    if (0 < zSmallCharsPerDot) {
      final String tx = "....................................................";
      final int len = 1 + text.length() / zSmallCharsPerDot;
      text = (len >= tx.length()) ? tx : tx.substring(0, len);
    }
    zGraphics.drawString(text, x, y);
    zGraphics.setColor(zColorTool);
  }

  @Override
  public int getBoundsLeft(String txt) {
    final Rectangle2D rect = getBounds(txt, zGraphics);
    return (int) rect.getX();
  }

  @Override
  public int getBoundsRight(String txt) {
    final Rectangle2D rect = getBounds(txt, zGraphics);
    return 1 + (int) (rect.getX() + rect.getWidth());
  }

  @Override
  public int getBoundsMiddle(String txt) {
    final Rectangle2D rect = getBounds(txt, zGraphics);
    return 1 + ((int) (rect.getX() + rect.getWidth()) >> 1);
  }

  @Override
  public void setCanvasImage(
      int index, int width, int height, int colorBackground, int colorBorder) {
    if (zImages.length <= index) {
      zImages = Arrays.copyOf(zImages, index + 8);
      zImageBorderColor = Arrays.copyOf(zImageBorderColor, index + 8);
    }
    zImageBorderColor[index] = colorBorder;
    // Leave one pixel for border line:
    if (0 != colorBorder) {
      --width;
      --height;
    }
    zImages[index] = createVolatileImage(width, height);
    zGraphics = (Graphics2D) zImages[index].getGraphics();
    final Color bg = getColor(colorBackground);
    zGraphics.setBackground(bg);
    zGraphics.setColor(bg);
    zGraphics.fillRect(0, 0, width, height);
    zGraphics.setColor((0x10 > (colorBackground & 0xff)) ? Color.WHITE : Color.BLACK);
  }

  @Override
  public void setClip(int left, int top, int right, int bottom) {
    zGraphics.setClip(left, top, right - left, bottom - top); // clipRect(left, top, right, bottom);
  }

  @Override
  public void setColorText(int xColor) {
    zColorText = getColor(xColor);
  }

  @Override
  public void setColorTool(int xColor) {
    zColorTool = getColor(xColor);
    if (null != zGraphics) {
      zGraphics.setColor(zColorTool);
    }
  }

  @Override
  public void setColor(int xColor) {
    setColorText(xColor);
    setColorTool(xColor);
  }

  @Override
  public int setMatching128Font(int xHeightPx, String font, int type) {
    if (null == this.zFontNamesMonoSysSerif[0]) {
      initFonts(zGraphics);
    }
    Font f =
        (null != font)
            ? new Font(font, Font.PLAIN, xHeightPx)
            : this.zFonts128MonoSysSerif[type % 3];
    return setMatching128FontHeight(f, xHeightPx, zGraphics).getSize();
  }

  @Override
  public int setMatching128FontHeight(int xHeightPx) {
    if (null == this.zFontNamesMonoSysSerif[0]) {
      initFonts(zGraphics);
    }
    return setMatching128FontHeight(zGraphics.getFont(), xHeightPx, zGraphics).getSize();
  }

  @Override
  public void show() {
    // Nothing to do.
  }

  @Override
  public void paint(Graphics xGr) {
    // No: super.paint( xGr);
    zGraphicsTmp = (Graphics2D) xGr;
    final Rectangle size = zGraphicsTmp.getClipBounds();
    if (Dib2Lang.AppState.ACTIVE.ordinal() > Dib2Root.app.appState.ordinal()) {
      FrameAwt.instance.checkSizeNZoom();
    } else if ((size.getWidth() != UiFrame.qWidthCanvasPx)
        || (size.getHeight() != UiFrame.qHeightCanvasPx)) {
      FrameAwt.instance.checkSizeNZoom();
    }
    if (!rFrame.paint4Frame(this)) {
      if (Dib2Lang.AppState.EXIT_REQUEST.ordinal() <= Dib2Root.app.appState.ordinal()) {
        FrameAwt.instance.closeFrame(true);
      }
    }
    zGraphicsTmp = null;
  }

  @Override
  public void update(Graphics g) {
    // No super.update(): avoid flicker.
    paint(g);
  }

  //////

  Font setMatching128FontHeight(Font f, int xHeightPx, Graphics2D g) {
    zSmallCharsPerDot = 0;
    Font out = null;
    final int refHeight = 128;
    boolean mono = f.getFontName().toUpperCase(Locale.ROOT).contains("MONO");
    out = f.deriveFont((float) ((7 <= xHeightPx) ? xHeightPx : 7));
    g.setFont(out);
    FontRenderContext frc = g.getFontRenderContext();
    TextLayout layout = new TextLayout("nxn", out, frc);
    Rectangle2D bb = layout.getBounds();
    int xh = (int) bb.getHeight();
    layout = new TextLayout(kDigits, out, frc);
    bb = layout.getBounds();
    final int widN = (int) bb.getWidth();
    layout = new TextLayout(kAlphaOmega, out, frc);
    bb = layout.getBounds();
    final int widA = (int) bb.getWidth();
    if ((7 > xHeightPx) || (4 > xh)) {
      xHeightPx = (0 < xHeightPx) ? xHeightPx : 1;
      zSmallCharsPerDot = 1 + (widN + kDigits.length() + 2) / (kDigits.length() * xHeightPx);
      return out;
    }

    int w0 = UiFunc.boundWidthMono(kDigits, refHeight);
    int w1 = widN;
    if (!mono) {
      int cmp = UiFunc.boundWidthNmz(kAlphaOmega, refHeight);
      w0 = UiFunc.boundWidthNmz(kDigits, refHeight);
      int percent = 100 * (widN * 128 / w0) / (widA * 128 / cmp);
      if (105 <= percent) {
        w0 += kDigits.length();
      } else if (percent <= 95) {
        percent = (150 < percent) ? 150 : ((70 > percent) ? 70 : percent);
        w0 = w0 * (percent + 100) / 2 / 100;
      }
    }
    if (w0 < w1) {
      w0 = w0 * xHeightPx / refHeight;
    } else {
      w1 = w1 * refHeight / xHeightPx;
    }
    if (w0 < w1) {
      w0 = UiFunc.boundWidthMax(kDigits, refHeight);
      if (w0 > w1) {
        return out;
      }
    }
    if ((w1 + kDigits.length()) < w0) {
      final int h2 = xHeightPx * (w0 - 1) / (w1 + 1);
      if (xh < (xHeightPx / 2 - 1)) {
        xHeightPx = (xHeightPx + h2 + 1) / 2;
      } else if (h2 > xHeightPx) {
        xHeightPx += (h2 > (xHeightPx + 1)) ? 2 : 1;
      }
    } else if (w1 > (w0 + 1)) {
      xHeightPx = xHeightPx * w0 / (w1 + 1);
      if (7 > xHeightPx) {
        xHeightPx = (0 < xHeightPx) ? xHeightPx : 1;
        zSmallCharsPerDot = 1 + (widN + kDigits.length() + 2) / (kDigits.length() * xHeightPx);
        return out;
      }
    }
    out = f.deriveFont((float) xHeightPx);
    g.setFont(out);
    g.setRenderingHint(
        RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON); // RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
    return out;
  }

  static Color getColor(int xColor) {
    int inx = zColorVals.length;
    for (--inx; inx >= 0; --inx) {
      if (xColor == zColorVals[inx]) {
        break;
      }
    }
    if (0 > inx) {
      ziColorNext = (ziColorNext + 1) % zColorVals.length;
      inx = ziColorNext;
    }
    if ((zColorVals[inx] != xColor) || (null == zColors[inx])) {
      zColorVals[inx] = xColor;
      zColors[inx] = new Color(xColor, true);
    }
    return zColors[inx];
  }

  static Rectangle2D getBounds(String txt, Graphics2D g) {
    if (0 >= txt.length()) {
      return new Rectangle(0, 0);
    } else if ((0 < txt.indexOf(' ')) && (0 >= txt.trim().length())) {
      return new Rectangle(0, 0);
    }
    final FontRenderContext frc = g.getFontRenderContext();
    final Font font = g.getFont();
    TextLayout layout = new TextLayout(txt, font, frc);
    return layout.getBounds();
  }

  static boolean hasSpecialChars(Font font, boolean sys) {
    char[] chs = sys ? StringFunc.kControlAsButton : StringFunc.ipa4Tipa_Offs30;
    for (char ch : chs) {
      if ((' ' < ch) && !font.canDisplay(ch)) {
        return false;
      }
    }
    return true;
  }

  // =====
}
