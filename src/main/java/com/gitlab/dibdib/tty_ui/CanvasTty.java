// Copyright (C) 2020, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.tty_ui;

import java.util.Arrays;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.GraphicsIf;

// =====

public class CanvasTty implements GraphicsIf {

  // =====

  public static final int kPx2CharShift = 2;

  // =====
  private static class Rectangle {
    // =====

    int height;
    int width;

    int getHeight() {
      return height;
    }

    int getWidth() {
      return width;
    }

    int getX() {
      return 0;
    }

    // =====
  }

  // =====
  private static class Image {
    // =====

    int height;
    int width;
    char[][] textDoubleLines;
    int colorBorder;

    // =====
  }

  private Image[] zImages = new Image[8];
  /** Char's per line with half a char's height as 4x4 px. */
  private int zWidth;
  /** Two lines combined: 76=>38, for text/ additional data/ pixel data. */
  private int zHeight;

  private char[][] zCanvasTextDoubleLines = null;
  private char[][] textDoubleLines;
  private int fontHeight;
  private int colorText;
  private int colorTool;
  private int clipX0;
  private int clipX1;
  private int clipY0;
  private int clipY1;

  public CanvasTty(int width, int height) {
    reset(width, height);
  }

  public void setCanvasFull() {
    fontHeight = 3 << kPx2CharShift;
    clipX0 = 0;
    clipX1 = zWidth;
    clipY0 = 0;
    clipY1 = zHeight;
    textDoubleLines = zCanvasTextDoubleLines;
  }

  public final void reset(int width, int height) {
    zWidth = width;
    zHeight = height;
    if ((null == zCanvasTextDoubleLines)
        || (zWidth < (zCanvasTextDoubleLines[0].length << kPx2CharShift))
        || (zHeight < (zCanvasTextDoubleLines.length << kPx2CharShift))) {
      zCanvasTextDoubleLines = new char[(((zHeight >> kPx2CharShift) >> 1) + 1) << 1][];
      for (int i0 = zCanvasTextDoubleLines.length - 1; i0 >= 0; --i0) {
        zCanvasTextDoubleLines[i0] = new char[(zWidth >> kPx2CharShift) + 1];
      }
    } else {
      for (int i0 = zCanvasTextDoubleLines.length - 1; i0 >= 0; --i0) {
        Arrays.fill(zCanvasTextDoubleLines[i0], (char) 0);
      }
    }
    setCanvasFull();
  }

  private final Rectangle getBounds_rectangle = new Rectangle();

  private Rectangle getBounds(String txt, CanvasTty g) {
    getBounds_rectangle.height = 0;
    getBounds_rectangle.width = 0;
    if (0 >= txt.length()) {
    } else if ((0 < txt.indexOf(' ')) && (0 >= txt.trim().length())) {
    } else {
      getBounds_rectangle.height = g.fontHeight;
      final int wid = txt.length() << kPx2CharShift;
      getBounds_rectangle.width =
          ((1 << kPx2CharShift) >= g.fontHeight) ? wid : ((g.fontHeight * txt.length()) >> 1);
    }
    return getBounds_rectangle;
  }

  @Override
  public int getBoundsLeft(String txt) {
    return getBounds(txt, this).getX();
  }

  @Override
  public int getBoundsRight(String txt) {
    return getBounds(txt, this).getWidth();
  }

  @Override
  public int getBoundsMiddle(String txt) {
    return getBounds(txt, this).getWidth() >> 1;
  }

  @Override
  public int setMatching128FontHeight(int xHeightPx) {
    fontHeight = xHeightPx;
    return xHeightPx;
  }

  @Override
  public int setMatching128Font(int xHeightPx, String font, int type) {
    return setMatching128FontHeight(xHeightPx);
  }

  @Override
  public void setClip(int xX, int xY, int xRight, int xBottom) {
    final int width = textDoubleLines[0].length << kPx2CharShift;
    clipX0 = (0 > xX) ? 0 : ((xX <= width) ? xX : width);
    clipX1 = xRight;
    clipX1 = (0 > clipX1) ? 0 : ((clipX1 <= width) ? clipX1 : width);
    final int height = textDoubleLines.length << kPx2CharShift;
    clipX0 = (0 > xY) ? 0 : ((xY <= height) ? xX : height);
    clipY1 = xBottom;
    clipY1 = (0 > clipY1) ? 0 : ((clipY1 <= height) ? clipY1 : height);
  }

  @Override
  public void setColorText(int xColor) {
    colorText = xColor;
  }

  @Override
  public void setColorTool(int xColor) {
    colorTool = xColor;
  }

  @Override
  public void setColor(int xColor) {
    setColorText(xColor);
    setColorTool(xColor);
  }

  private void setPixel(int xX, int xY) {
    final int cx = xX >> kPx2CharShift;
    final int cy = xY >> kPx2CharShift;
    char bit =
        (char)
            (0x10
                | 1
                    << ((((kPx2CharShift & xY) != 0) ? 2 : 0)
                        | (((kPx2CharShift & xX) != 0) ? 1 : 0)));
    if ((clipX0 <= xX) && (xX < clipX1) && (clipY0 <= xY) && (xY < clipY1)) {
      char old = textDoubleLines[cy][cx];
      if (' ' > old) {
        textDoubleLines[cy][cx] = (char) (old | bit);
      }
    }
  }

  @Override
  public void drawLine(int xX0, int xY0, int xX1, int xY1) {
    final int cX = xX1 - xX0;
    final int cY = xY1 - xY0;
    int steps = (cX > cY) ? cX : cY;
    for (int i0 = 0; i0 < steps; ++i0) {
      setPixel(xX0 + (i0 * cX) / steps, xY0 + (i0 * cY) / steps);
    }
  }

  @Override
  public void drawText(String txt, int xX, int xY) {
    int cx = xX >> kPx2CharShift;
    int cy = xY >> kPx2CharShift;
    int cChars =
        ((1 << kPx2CharShift) >= fontHeight)
            ? txt.length()
            : ((fontHeight * txt.length()) >> (1 + kPx2CharShift));
    final int len = txt.length();
    if ((0 >= len) || (xY < clipY0) || (clipY1 <= xY)) {
      return;
    }
    ///// Center glyphs, ' ' as delimiter left ...
    final int eTransparent = (0xee > (colorText >>> 24)) ? 1 : 0;
    cy += eTransparent - (((fontHeight >> kPx2CharShift) >> 2) << 1);
    cx += (fontHeight >> kPx2CharShift) >> 2;
    cChars -= (fontHeight >> kPx2CharShift) >> 1;
    cy = (0 > cy) ? 0 : ((textDoubleLines.length > cy) ? cy : (textDoubleLines.length - 1));
    if (((clipX0 >> kPx2CharShift) <= (cx - 1)) && (cx <= (clipX1 >> kPx2CharShift))) {
      textDoubleLines[cy][cx - 1] = ' ';
    }
    int x0 = cx;
    int eX = (cChars <= len) ? 1 : (cChars / len);
    for (int i0 = 0; i0 < len; ++i0, x0 += eX) {
      char ch = txt.charAt(i0);
      if (' ' >= ch) {
        ch = ' ';
        final int next = cx + i0 * cChars / len;
        --x0;
        if (next == x0) {
          ++x0;
        } else {
          if ((cChars < len)
              && (next <= x0)
              && !((clipX0 >> kPx2CharShift) > x0)
              && !((clipX1 >> kPx2CharShift) <= x0)) {
            if ((0 < i0) && (' ' != txt.charAt(i0 - 1))) {
              textDoubleLines[cy][x0] = '^';
              continue;
            }
          }
          x0 = next;
        }
      }
      if ((clipX0 >> kPx2CharShift) > x0) {
        continue;
      } else if ((clipX1 >> kPx2CharShift) <= x0) {
        break;
      }
      textDoubleLines[cy][x0] = ch;
    }
  }

  private void drawImage_clip_DEPR(Image cnv, int xX, int xY) {
    int x0 = (clipX0 > xX) ? clipX0 : xX;
    final int x1 = (clipX1 < (xX + cnv.width)) ? clipX1 : (xX + cnv.width);
    int y0 = (clipY0 > xY) ? clipY0 : xY;
    final int y1 =
        (clipY1 < (xY + (cnv.textDoubleLines.length << kPx2CharShift)))
            ? clipY1
            : (xY + (cnv.textDoubleLines.length << kPx2CharShift));
    int iyI = (y0 - xY) >> kPx2CharShift;
    for (int iY = (y0 >> kPx2CharShift) & ~1;
        (iyI < cnv.textDoubleLines.length) && (iY < (y1 >> kPx2CharShift));
        ++iyI, ++iY) {
      int ixI = (x0 - xX) >> kPx2CharShift;
      for (int iX = x0 >> kPx2CharShift;
          (ixI < (cnv.width >> kPx2CharShift)) && (iX < x1);
          ++ixI, ++iX) {
        textDoubleLines[iY][iX] = cnv.textDoubleLines[iyI][ixI];
      }
    }
  }

  private void drawImage(Image cnv, int xX, int xY) {
    int iCY = xY >> kPx2CharShift;
    for (int iIY = 0;
        (iIY < cnv.textDoubleLines.length) && (iCY < textDoubleLines.length);
        ++iIY, ++iCY) {
      if (0 > iCY) {
        iIY -= iCY + 1;
        iCY = -1;
        continue;
      }
      int iCX = xX >> kPx2CharShift;
      for (int iIX = 0;
          (iIX < cnv.textDoubleLines[iIY].length) && (iCX < textDoubleLines[iCY].length);
          ++iIX, ++iCX) {
        if (0 > iCX) {
          iIX -= iCX + 1;
          iCX = 0;
          continue;
        }
        textDoubleLines[iCY][iCX] = cnv.textDoubleLines[iIY][iIX];
      }
    }
  }

  @Override
  public void setCanvasImage(
      int index, int width, int height, int colorBackground, int colorBorder) {
    //  createVolatileImage(widthFrame, heightCanvas);
    if (zImages.length <= index) {
      zImages = Arrays.copyOf(zImages, index + 8);
    }
    if (null == zImages[index]) {
      zImages[index] = new Image();
    }
    if ((null == zImages[index].textDoubleLines) || (height > zImages[index].height)) {
      zImages[index].textDoubleLines = new char[(((height >> kPx2CharShift) >> 1) + 1) << 1][];
    }
    textDoubleLines = zImages[index].textDoubleLines;
    for (int i0 = textDoubleLines.length - 1; i0 >= 0; --i0) {
      if ((null == textDoubleLines[i0])
          || (width < (textDoubleLines[i0].length << kPx2CharShift))) {
        textDoubleLines[i0] = new char[(width >> kPx2CharShift) + 1];
      } else {
        Arrays.fill(textDoubleLines, 0);
      }
    }
    setClip(0, 0, width, height);
    zImages[index].colorBorder = colorBorder;
    zImages[index].height = height;
    zImages[index].width = width;
  }

  @Override
  public void drawImage(int index, int left, int top) {
    setCanvasFull();
    drawImage(zImages[index], left, top);
  }

  @Override
  public void show() {
    setCanvasFull();
    FrameTty fr = FrameTty.instance;
    char[] out = new char[(zWidth >> kPx2CharShift) + 1];
    System.out.println();
    final String s5 = "     ";
    final String s20 = s5 + s5 + s5 + s5;
    final String s80 = s20 + s20 + s20 + s20;
    int cMouseX = fr.qMouseX >> kPx2CharShift;
    for (int i0 = 0; i0 < out.length; ++i0) {
      if ((i0 % 10) == 8) {
        out[i0] = ' ';
      } else if ((i0 % 10) == 9) {
        out[i0] = (char) (((i0 / 10 + 1) % 10) + '0');
      } else {
        out[i0] = (char) ((i0 % 10) + '0');
      }
    }
    System.out.println("00 " + new String(out));
    int count = 0;
    for (int line = 0; line < textDoubleLines.length; ++line) {
      int txt = 0;
      int px = 0;
      Arrays.fill(out, (char) 0);
      for (int i0 = 0; i0 < textDoubleLines[line].length; ++i0) {
        final char ch = textDoubleLines[line][i0];
        out[i0] = ch;
        txt += (' ' < ch) ? 1 : 0;
        px += ((0 != ch) && (' ' > ch)) ? 1 : 0;
      }
      if ((1 >= txt) && (2 >= px) && ((count + 2) >= (line >> 1))) {
        continue;
      }
      ++count;
      if ((3 > txt) || (count > (line >> 1))) {
        ++line;
        if (textDoubleLines.length > line) {
          for (int i0 = 0; i0 < textDoubleLines[line].length; ++i0) {
            final char ch = textDoubleLines[line][i0];
            if (' ' >= out[i0]) {
              out[i0] = ((0 == out[i0]) || (' ' < ch)) ? ch : (char) (out[i0] | ch);
            } else if (' ' < ch) {
              if ((0 < i0) && (' ' >= out[i0 - 1])) {
                out[i0 - 1] = ch;
              } else if (((i0 + 1) < out.length) && (' ' >= out[i0 + 1])) {
                out[i0 + 1] = ch;
              } else {
                out[i0] = '^';
              }
            }
          }
        }
      }
      if ((line >= (fr.qMouseY >> kPx2CharShift)) && (0 <= cMouseX) && (cMouseX < out.length)) {
        out[cMouseX] = '\u00a4';
        cMouseX = -1;
      }
      for (int i0 = 0; i0 < textDoubleLines[line].length; ++i0) {
        if (' ' > out[i0]) {
          final int bits = textDoubleLines[line][i0] & 0xf;
          final int upper = (0 != (bits & 3)) ? 2 : 0;
          final int lower = (0 != (bits & 0xc)) ? 1 : 0;
          out[i0] =
              (FrameTty.asciiOnly ? " \"_#".toCharArray() : PlatformFunc.kBoxes)[upper + lower];
        } else if (FrameTty.asciiOnly && (0x7f <= out[i0])) {
          out[i0] = (0xc0 <= out[i0]) ? '^' : '*';
        }
      }
      String s0 = new String(out);
      if (FrameTty.useCarriageReturn) {
        ///// Some systems have problems with spacing for Unicode char's ...!
        if (0 < s0.trim().length()) {
          for (int i0 = out.length - 1; i0 >= 0; --i0) {
            if (' ' >= out[i0]) {
              continue;
            }
            System.out.print('\r');
            System.out.print(s80.substring(0, i0 + 3));
            System.out.print(out[i0]);
          }
          System.out.print('\r');
        }
        s0 = "";
      }
      System.out.println(String.format("%02d %s", line + 1, s0));
    }
  }

  // =====
}
