// Copyright (C) 2020, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.tty_ui;

import net.sf.dibdib.config.*;
import net.sf.dibdib.thread_feed.*;
import net.sf.dibdib.thread_ui.*;

// =====

public class FrameTty {

  // =====

  public static boolean useCarriageReturn = false;
  public static boolean asciiOnly = false;

  public static FrameTty instance;
  private final CanvasTty mCanvas;
  private final UiFrame mPaint;
  private final int mWidPt10;

  ///// 38/40 lines, 76/80 chars.

  /** 76 chars at 72/8*4/2 'dpi' (4x4 px per char) => as if ~8 inch at ~300 px. */
  static final int kShift2Px0 = Dib2Constants.UI_PT10_SHIFT + 2 - CanvasTty.kPx2CharShift;

  int qMouseX = 0;
  int qMouseY = 0;

  protected static String path = "dibdib.dm";

  public FrameTty(String title, int charsPerLine, int lines) {
    qMouseX = (charsPerLine / 2) << CanvasTty.kPx2CharShift;
    // Extra pixel for border line:
    UiFrame.qWidthCanvasPx = (charsPerLine << CanvasTty.kPx2CharShift) + 1;
    // Calculate as double lines:
    UiFrame.qHeightCanvasPx = ((2 * (lines / 2)) << CanvasTty.kPx2CharShift) + 1;
    mWidPt10 = charsPerLine * Dib2Constants.UI_PT10_P_INCH / 10 / 2;
    mCanvas = new CanvasTty(UiFrame.qWidthCanvasPx, UiFrame.qHeightCanvasPx);
    mPaint = new UiFrame();
    path = (null == Dib2Root.CmdLineArgs.X0.value) ? path : Dib2Root.CmdLineArgs.X0.value;
    useCarriageReturn = (null != Dib2Root.CmdLineArgs.CARRIAGERETURN.value);
    asciiOnly = (null != Dib2Root.CmdLineArgs.ASCII.value);
    instance = this;
  }

  void checkSizeNZoom() {
    //    final int zoom = UiValTag.UI_ZOOMLVL_DISPLAY.i32(0);
    // Additional pixel for border line had been set.
    UiFrame.checkSizeNZoom(UiFrame.qWidthCanvasPx, UiFrame.qHeightCanvasPx, mWidPt10);
  }

  protected void closeFrame(boolean actively) {
    boolean ok = true;
    if (null != Dib2Root.CmdLineArgs.DEBUG.value) {
      ok = Dib2Root.onCloseApp(3500);
    }
    if (actively) {
      System.exit(ok ? 0 : 1);
    }
  }

  public void mouseMoveToChar(int xX, int xY) {
    qMouseX = xX << CanvasTty.kPx2CharShift;
    qMouseY = xY << CanvasTty.kPx2CharShift;
  }

  public void mouseClick() {
    UiPres.INSTANCE.handleMouse(qMouseX, qMouseY);
  }

  /////

  public int getInitialPx4Pt10ShiftVal() {
    return kShift2Px0;
  }

  private int invalidate_count = 0;

  public void invalidate() {
    if (MainThreads.isIdle()) {
      invalidate_count = 0;
      mCanvas.reset(UiFrame.qWidthCanvasPx, UiFrame.qHeightCanvasPx);
      checkSizeNZoom();
      mPaint.paint4Frame(mCanvas);
    } else {
      System.out.print('.');
      if (75 < ++invalidate_count) {
        System.out.println("");
        invalidate_count = 0;
      }
    }
  }

  // =====
}
