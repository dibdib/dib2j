// Copyright (C) 2020, 2021  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (or www.gnu.org/licenses/gpl.html),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Netbeans Ext plugin.

package com.gitlab.dibdib.tty_ui;

import java.io.*;
import java.net.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.config.Dib2Lang.AppState;
import net.sf.dibdib.config.Dib2Root.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.generic.QIfs.TsvCodecIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.UiPres;

// =====

public class AppTty extends PlatformFunc implements Runnable {

  // =====

  public static String name = "Dib2Cmd";
  public static FrameTty frame;

  protected BufferedReader in;
  protected String fallbackClipboard = "";

  protected static Thread[] qThreads = null;
  protected static TreeSet<String> entries;

  public void create(
      String title,
      int charsPerLine,
      int lines,
      char xPlatformMarker,
      String xAppShort,
      QIfs.PlatformIf xPlatform,
      Runnable xWorker,
      TsvCodecIf[] xmCsvCodecs) {
    Dib2Root.app.bPermitted = true;
    frame = new FrameTty(title, charsPerLine, lines);
  }

  public void configure() {
    Dib2Root.init(false);
    frame.checkSizeNZoom();
    Dib2Root.resume();
  }

  private static void promptFull() {
    System.out.println("Type ';;' (plus ENTER) to end the program, '?' to get some info/ help,");
    System.out.println("'?!' for the license, or ';' plus command.");
    System.out.println("(Sample usage: type '3', press ENTER, '4', ENTER, ';+', ENTER)");
    System.out.println("(OR: type '3', press ENTER, '4', ENTER, ';ADD', ENTER)");
    System.out.println("(Sample 2: type '\\' + name of new file, press ENTER, ';EXPORT', ENTER)\n");
    System.out.println("Press just ENTER to get started.\n");
  }

  private static TreeSet<String> toEntrySet(String dat) {
    TreeSet<String> out = new TreeSet<String>();
    int i1 = 0;
    for (int i0 = 0; 0 <= (i1 = dat.indexOf('\n', i0)); i0 = i1 + 1) {
      String[] els = dat.substring(i0, i1).split("\t");
      if ((3 >= els.length) || (16 >= els[3].length())) {
        continue;
      }
      if (7 < els.length) {
        if (("PREF".equals(els[2])) || ("VAR".equals(els[2]))) {
          out.add(els[1] + els[2] + "...." + els[7].trim().replaceAll("[^0-9A-F]", ""));
        } else {
          out.add(els[1] + els[2] + els[3].substring(0, 16) + els[7].trim());
        }
      } else { // if (3 < els.length) {
        out.add(els[1] + els[2] + els[3].substring(0, 16));
      }
    }
    return out;
  }

  private static void cmpData(String marker, TreeSet<String> d0, TreeSet<String> d1) {
    for (String entry : d1) {
      if (!d0.contains(entry)) {
        System.out.println("DIFF" + marker + "\t" + entry);
        for (String xcmp : d0) {
          if ((9 < xcmp.length()) && entry.startsWith(xcmp.substring(0, 9))) {
            System.out.println("\n////?\t" + xcmp);
          }
        }
      }
    }
  }

  protected static boolean getData(String phrase, String accessCode) {
    System.out.println(
        "Trying to decode data from " + new File(Dib2Root.app.dbFileName).getAbsolutePath());
    System.out.println("\n...\n");

    byte[] header = new byte[8];
    byte[] dat = TcvCodec.instance.readPacked(Dib2Root.app.dbFileName, header, phrase, accessCode);

    if (1 == (header[0] & 0xff)) {
      entries = new TreeSet<String>();
    } else if (Dib2Constants.MAGIC_BYTES[0] != (header[0])) {
      System.out.println("Cannot decode file: " + Dib2Root.app.dbFileName);
      return false;
    } else {
      String datx = new String(dat, StringFunc.CHAR16UTF8);
      System.out.print(datx);
      entries = toEntrySet(datx);
      System.out.println("\n\n=====");
      int version = header[2] & 0xff;
      // int cnt = Dib2Root.ccmSto.importCsv(dat, true, (4 >= version) ? 4 : 0);
      // System.out.println("Records: " + cnt);
      System.out.println("=====\n");
    }
    return true;
  }

  private void printLines(String[] lines, int len, String pre, boolean cut) {
    for (int i0 = 0; i0 < len; ++i0) {
      String line = "";
      if ((lines.length > i0) && (null != lines[i0])) {
        line = lines[i0].replace("\n", " // ");
        if ((null != line) && cut && (75 <= line.length())) {
          line = line.substring(0, 70) + "...";
        }
        line = StringFunc.makePrintable(line);
      }
      System.out.println((null == line) ? "" : pre + line);
    }
  }

  public static boolean dumpHelp() {
    if (null != CmdLineArgs.HELP.value) {
      System.out.println(Dib2Root.getHelp(name));
      return true;
    } else if (null != CmdLineArgs.VERSION.value) {
      System.out.println(Dib2Root.getVersionInfo(name));
      return true;
    }
    return false;
  }

  public boolean start() {
    System.out.println(name + " version " + Dib2Constants.VERSION_STRING);
    System.out.println(Dib2Constants.NO_WARRANTY[0] + ' ' + Dib2Constants.NO_WARRANTY[1]);
    System.out.println("Requires proper security settings for Java ! (.../jre/lib/security)\n");
    System.out.println("Really! :-)\nTry it out, but do not be surprised ...");
    System.out.println(
        "If you get some kind of decoding error after the next steps, it could very well");
    System.out.println("be your Java settings.\n");

    try {
      System.out.println("Access code (for file " + Dib2Root.app.dbFileName + ")?");
      in = new BufferedReader(new InputStreamReader(System.in, "UTF8"));
      String accessCode = in.readLine().trim();
      TcvCodec.instance.setAccessCode(StringFunc.bytesUtf8(accessCode));
      System.out.println("(E-mail) password?");
      String phrase = in.readLine().trim();
      TcvCodec.instance.settleHexPhrase(StringFunc.hexUtf8(phrase, false));
      for (int i0 = phrase.length() - 1; i0 >= 0; --i0) {
        if (' ' > phrase.charAt(i0)) {
          phrase = phrase.substring(0, i0) + phrase.substring(i0 + 1);
        }
      }
      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n...\n");
      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n...\n");
      if (new File(Dib2Root.app.dbFileName).isFile()) {
        if (!getData(phrase, accessCode)) {
          return false;
        }
      }
      //      Dib2Root.initAsync();
    } catch (Exception e) {
      System.out.println("Exception: " + e);
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private int getDigits_value = 0;

  private int getDigits(String x0, int i0) {
    int digits = i0;
    for (; digits < x0.length(); ++digits) {
      final char kx = x0.charAt(digits);
      if ((kx < '0') || ('9' < kx)) {
        break;
      }
    }
    try {
      getDigits_value = 0;
      if (digits > i0) {
        char k0 = (5 < (digits - i0)) ? 0 : (char) Integer.parseInt(x0.substring(i0, digits));
        i0 = digits;
        getDigits_value = k0;
      }
    } catch (Exception e) {
    }
    return i0;
  }

  void handleCtrl(String x0) {
    boolean ctrl = true;
    for (int i0 = 1; i0 < x0.length(); ++i0) {
      char k0 = x0.charAt(i0);
      switch (k0) {
        case ',':
          ctrl = !ctrl;
          continue;
        case '.':
          {
            final int digitsX = getDigits(x0, i0 + 1);
            if (digitsX > (i0 + 1)) {
              ++i0;
              final int vX = getDigits_value;
              int jX = frame.qMouseX;
              int jY = frame.qMouseY;
              final int digitsY = getDigits(x0, digitsX + 1);
              if ((digitsY > (digitsX + 1)) && (digitsY < x0.length())) {
                if ('+' == x0.charAt(digitsY)) {
                  jY += getDigits_value;
                } else if ('-' == x0.charAt(digitsY)) {
                  jY -= getDigits_value;
                } else { // '.', '@', ...
                  jY = getDigits_value;
                }
              }
              if ('+' == x0.charAt(digitsX)) {
                jX += vX;
              } else if ('-' == x0.charAt(digitsX)) {
                jX -= vX;
              } else { // '.', '@', ...
                jX = vX;
              }
              frame.mouseMoveToChar(jX, jY);
            } else if (((i0 + 1) < x0.length()) && ('.' == x0.charAt(i0 + 1))) {
              frame.mouseClick();
            }
          }
          continue;
        case ':':
        case ';':
        case '#':
          if (ctrl) {
            continue;
          }
          break;
        case '\'':
        case '-':
          k0 = ctrl ? ',' : k0;
          break;
        case '\\':
          k0 = x0.charAt((x0.length() > (i0 + 1)) ? (i0 + 1) : i0);
          break;
        default:
          if (ctrl) {
            int digits = getDigits(x0, i0);
            if (digits > i0) {
              k0 = (char) getDigits_value;
              i0 = digits - 1;
            } else {
              k0 = (char) (k0 & 0x1f);
            }
          }
      }
      UiPres.INSTANCE.handleKey(k0, false);
    }
  }

  /** Leading '\\'/ acute for quoted line, ';'/ sharp-S for command, ',' for CTRL, ':' RFU. */
  public void run() {
    promptFull();
    ((net.sf.dibdib.thread_feed.FeederRf) Dib2Root.app.feederCurrent).get().prepareFeed();
    while (Dib2Root.app.appState.ordinal() < AppState.EXIT_REQUEST.ordinal()) {
      System.out.print('>');
      String x0 = "";
      try {
        x0 = in.readLine();
      } catch (IOException e) {
      }
      if (0 == x0.length()) {
        invalidate();
        continue;
      }
      switch (x0.charAt(0)) {
        case '\u00DF': // sharp-S
        case ';':
          if ((2 == x0.length()) && (x0.charAt(0) == x0.charAt(1))) {
            // Dib2Root.app.exitRequestTraced = ExceptionAdapter.EXIT;
            Dib2Root.app.appState = AppState.EXIT_REQUEST;
            continue;
          }
          UiPres.INSTANCE.setEntry(x0.substring(1));
          UiPres.INSTANCE.handleKey(StringFunc.STEP, false);
          continue;
        case ',':
          handleCtrl(x0);
          continue;
        case ':':
          continue;
        case '?':
          continue;
        case '\u00B4': // acute
        case '\\':
          UiPres.INSTANCE.setEntry(x0.substring(1));
          break;
        default:
          UiPres.INSTANCE.setEntry(x0);
      }
      UiPres.INSTANCE.handleKey(StringFunc.PUSH, false);
    }
    Dib2Root.app.appState = AppState.EXIT_REQUEST;
    Thread.yield();
    frame.closeFrame(false);
    System.out.println("Good-by.");
  }

  @Override
  public void log(String... aMsg) {
    if (null != Dib2Root.CmdLineArgs.DEBUG.value) {
      System.out.println(Arrays.toString(aMsg));
    }
  }

  @Override
  public void toast(String msg) {
    System.out.println(msg);
  }

  @Override
  public boolean pushClipboard(String label, String text) {
    fallbackClipboard = text;
    return false;
  }

  @Override
  public String getClipboardText() {
    return fallbackClipboard;
  }

  @Override
  public void invalidate() {
    frame.invalidate();
  }

  @Override
  public InputStream openInputStreamGetType(Object xUri, String[] yType) {
    return null;
  }

  @Override
  public Object parseUri(String path) {
    try {
      return new URI(path);
    } catch (URISyntaxException e) {
    }
    return null;
  }

  // =====
}
